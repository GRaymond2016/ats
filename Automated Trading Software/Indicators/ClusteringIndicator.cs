﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Utilities;
using Automated_Trading_Software.Base_Classes;
using Automated_Trading_Software.Strategies;
using System.IO;
using Automated_Trading_Software.Risk;

namespace Automated_Trading_Software.Indicators
{
    public class ClusteringIndicator : Indicator, Observable<ClusteringStructure>, Observable<TradingStructure>
    {
        private static Risk.RiskAnalyser m_analyser = new Risk.NaiveRiskAnalysis();
        private static object m_analyserLock = new object();

        public void RunFunctorOnAnalyser(Action<Risk.RiskAnalyser> functor)
        {
            lock (m_analyser)
            {
                functor.Invoke(m_analyser);
            }
        }

        private ObservedClass<ClusteringStructure> observable = new ObservedClass<ClusteringStructure>();
        public void addObserver(Action<ClusteringStructure> entry)
        {
            observable.addObserver(entry);
        }
        private void notifyObservers(ClusteringStructure entry)
        {
            observable.notifyObservers(entry);
        }
        private ObservedClass<TradingStructure> observable2 = new ObservedClass<TradingStructure>();
        public void addObserver(Action<TradingStructure> entry)
        {
            observable2.addObserver(entry);
        }
        private void notifyObservers(TradingStructure entry)
        {
            observable2.notifyObservers(entry);
        }

        private SwingTracker m_tracker = null;
        private int m_barcount = 0;
        //Origin bar index is our primary key here
        private Dictionary<int, ClusteringStructure> structures = new Dictionary<int, ClusteringStructure>();
        private string m_name;

        public double BarResolution { get; set; }
        public String InstrumentName { get { return m_name; } }
        public int Sector { get; set; }

        public Func<SwingState> GetCurrentState { private get; set; }
        public double TickSize { get; set; }
        public double CloseCount { get; set; }

        public double XAtoABRatio { get; set; }
        public double ABtoBCRatio { get; set; }
        public double BCtoCDRatio { get; set; }

        public bool RequireEquality { get; set; }
        public bool RequireClusterBounds { get; set; }

        public double ABFibHighRetracement { get; set; }
        public double ABFibLowRetracement { get; set; }

        public static float ClusterHigh { get; set; }
        public static float ClusterLow { get; set; }

        public ClusteringIndicator(ref SwingTracker track, string name)
        {
            DLLInit.Initialize();

            m_tracker = track;
            m_name = name;
            XAtoABRatio = 0.1;
            ABtoBCRatio = 0.5;
            BCtoCDRatio = 0.5;
            RequireEquality = false;
            Sector = 0;
            BarResolution = 0;
        }

        public void Begin()
        {
            
        }
        public void CheckForEndingStructures(ref ClusteringStructure struc, ClusteringStructure new_add)
        {
            if (struc.D == null &&
                new_add.B == struc.C && new_add.A == struc.B &&
                new_add.X == struc.A && new_add.Origin == struc.X)
            {
                struc.D = new_add.C;
                notifyObservers(struc);
            }
        }
        public void CheckForTradableStructure(ref ClusteringStructure sl, int bar, BarValue<double> currentbar)
        {
            if (sl.NextZone != BuyPosition.NotPurchased && sl.ClusterHits >= CloseCount)
                return;

            PurchasingRuleSet rule_set = null;
            lock (m_analyserLock)
            {
                rule_set = m_analyser.AssessStrategy(InstrumentName, TickSize, sl.MathematicalPrecision, Sector);
            }

            if (rule_set == null)
                return;

            foreach (PurchaseRule trade_rule in rule_set.PurchasingRules)
            {
                bool wasDnull = sl.D == null;
                if (wasDnull)
                {
                    sl.D = new BarEntry() { BarData = currentbar, BarLocation = bar };
                }

                TradePoint trade = trade_rule.CriteriaForAction(ref sl, bar, currentbar.Close);
                if (trade.Price > 0) //We have changed zone
                {
                    TradingStructure structure = new TradingStructure();

                    lock (m_analyserLock)
                    {
                        PurchaseTracker.AdviseUnitsToPurchase(m_analyser, ref trade);
                    }
                    //Assign first stop loss
                    trade.StopLoss = rule_set.StopLossRule.StopLoss(ref sl, trade.Price);
                    structure.Purchase = trade;
                    trade.ClusterEnd = sl.Cluster.Right;

                    foreach (SaleRule sale_rule in rule_set.SellingRules)
                    {
                        if (!structure.SalePoints.ContainsKey(sale_rule.SellPosition))
                        {
                            TradePoint point = sale_rule.CriteriaForAction(ref sl, TickSize);
                            if (point.Rule != null)
                            {
                                point.Units = trade.Units; //We begin calculation with the total number of units
                                //Assign the units to sell
                                PurchaseTracker.AdviseUnitsToSell(ref point);
                                //Assign stop loss associated with this sale
                                point.StopLoss = rule_set.StopLossRule.StopLoss(ref sl, point.Price);
                                if (point.Units > 0)
                                    structure.SalePoints.Add(sale_rule.SellPosition, point);
                            }
                        }
                    }

                    if (structure.SalePoints.Count == 0)
                        throw new InvalidOperationException("Sale points are misconfigured because we have no current sale points.");

                    structure.Purchase.MaxPrice = (structure.SalePoints.First().Value.Price - structure.Purchase.Price) / 2 + structure.Purchase.Price;

                    //Notify all observers to make a trade
                    structure.Purchase.ShortSale = sl.ShortSale;

                    lock (m_analyserLock)
                    {
                        MarketKnowledge knowledge = new MarketKnowledge();
                        knowledge.SecurityName = InstrumentName;
                        knowledge.Expiry = DateTime.Now.AddMinutes(BarResolution * (sl.Cluster.Right - bar));
                        knowledge.TimeGiven = DateTime.Now;
                        knowledge.Resolution = TimeSpan.FromMinutes(BarResolution);
                        knowledge.Sector = Sector;
                        m_analyser.AddKnowledge(knowledge);
                    }
                    notifyObservers(structure);
                }

                if (wasDnull)
                    sl.D = null;
            }
        }
        public void AnalyseStructures(ClusteringStructure sl, int bar, BarValue<double> lastbar)
        {
            for (int i = 0; i < structures.Count; ++i)
            {
                ClusteringStructure structure = structures.ElementAt(i).Value;
                if (structure.Purchased != ClusteringStructure.Invalidated && !CheckForInvalidStructures(i, structure, sl, lastbar.Close))
                {
                    CheckForTradableStructure(ref structure, bar, lastbar);
                    CheckForEndingStructures(ref structure, sl);
                }
            }
        }
        //Bit of a hack with the index being passed. Don't hate me.
        public bool CheckForInvalidStructures(int index, ClusteringStructure sl, ClusteringStructure struc, double lastDistance)
        {
            if (struc.Origin == sl.Origin && sl.Purchased == ClusteringStructure.Unpurchased) //Same Entry
            {
                if (sl.C.SwingType == SwingState.HighSwing && (sl.Cluster.Bottom > lastDistance || (sl.D != null && sl.X.BarData.Low > sl.D.BarData.Low)) ||
                    sl.C.SwingType == SwingState.LowSwing && (sl.Cluster.Top < lastDistance || (sl.D != null && sl.X.BarData.High < sl.D.BarData.High)))
                {
                    structures[struc.Origin.BarLocation].Purchased = ClusteringStructure.Invalidated;
                    return true;
                }
            }
            return false;
        }
        
        public void BarUpdate(BarValue<double> CurrentBar)
        {
            SwingEntry C = m_tracker.LastAgo(m_name),
                       B = m_tracker.LastAgo(m_name, 2),
                       A = m_tracker.LastAgo(m_name, 3),
                       X = m_tracker.LastAgo(m_name, 4),
                       O = m_tracker.LastAgo(m_name, 5);

            if (C.Valid() && B.Valid() && A.Valid() && X.Valid() && O.Valid())
            {
                ClusteringStructure sl = new ClusteringStructure()
                {
                    Origin = O,
                    B = B,
                    C = C,
                    A = A,
                    X = X,
                    D = null
                };

                double OXDistance = O.DistanceTo(X, TickSize), 
                        XADistance = X.DistanceTo(A, TickSize),
                        ABDistance = A.DistanceTo(B, TickSize),
                        BCDistance = B.DistanceTo(C, TickSize),
                        CDDistance = C.DistanceTo(m_barcount, CurrentBar.Low, TickSize);

                double axfiblow = A.FibonacciRetracements(X, ABFibLowRetracement),
                        axfibhigh = A.FibonacciRetracements(X, ABFibHighRetracement);

                sl.AXLowRetracement = Math.Min(axfiblow, axfibhigh);
                sl.AXHighRetracement = Math.Max(axfiblow, axfibhigh);

                if (CDDistance > BCDistance && BCDistance * BCtoCDRatio < CDDistance &&
                    ABDistance > BCDistance && ABDistance * ABtoBCRatio < BCDistance &&
                    ABDistance < XADistance && XADistance * XAtoABRatio < ABDistance &&
                    OXDistance < XADistance && sl.AXLowRetracement < B.GetAssociatedSwingValue() && sl.AXHighRetracement > B.GetAssociatedSwingValue())
                {
                    double bcfibhigh = C.FibonacciRetracements(B, ClusterHigh),
                            bcfiblow = C.FibonacciRetracements(B, ClusterLow);
                    sl.Cluster = new Rectangle<double>()
                    {
                        Bottom = Math.Min(bcfibhigh, bcfiblow),
                        Top = Math.Max(bcfibhigh, bcfiblow),
                        Left = C.BarLocation,
                        Right = C.BarLocation + (B.BarLocation - A.BarLocation) * 4
                    };
                    double xafibhigh = A.FibonacciRetracements(X, 76.4),
                            xafiblow = A.FibonacciRetracements(X, 50);

                    sl.ClusterMin = Math.Min(xafibhigh, xafiblow);
                    sl.ClusterMax = Math.Max(xafibhigh, xafiblow);
                    sl.Equality = sl.C.SwingType == SwingState.HighSwing ? sl.C.BarData.High - ABDistance * TickSize : sl.C.BarData.Low + ABDistance * TickSize;

                    if(sl.Cluster.Hit(m_barcount, CurrentBar.Close) &&
                        (!RequireClusterBounds || ((sl.Cluster.Top < sl.ClusterMax && sl.Cluster.Top > sl.ClusterMin) ||
                        (sl.Cluster.Bottom > sl.ClusterMin && sl.Cluster.Bottom < sl.ClusterMax))) &&
                        (!RequireEquality || (sl.Equality < sl.Cluster.Top && sl.Equality > sl.Cluster.Bottom)))
                    {
                        var value = structures.FirstOrDefault(x => x.Key == O.BarLocation).Value;
                        if (value == null)
                        {
                            structures.Add(sl.Origin.BarLocation, sl);
                            value = sl;
                        }
                        value.ClusterHits++;
                        if (value.ClusterHits >= CloseCount)
                        {
                            notifyObservers(sl);
                        }
                    }
                }

                AnalyseStructures(sl, m_barcount, CurrentBar);
            }
            m_barcount++;
        }

        public void Terminate()
        {
            
        }
    }
}
