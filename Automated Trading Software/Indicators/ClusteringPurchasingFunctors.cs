﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Base_Classes;

namespace Automated_Trading_Software.Indicators
{
    public abstract class PurchaseRule
    {
        public BuyPosition BuyPosition { get; protected set; }
        public abstract TradePoint CriteriaForAction(ref ClusteringStructure sl, int bar, double currentClose);
    }
    public abstract class SaleRule
    {
        public BuyPosition SellPosition { get; protected set; }
        public double SalePercent { get; protected set; }
        public abstract TradePoint CriteriaForAction(ref ClusteringStructure sl, double TickSize);
    }
    public abstract class StopLossRule
    {
        public abstract double StopLoss(ref ClusteringStructure sl, double sale_price);
    }
    abstract class ClusterClosePurchaseRule : PurchaseRule
    {
        public ClusterClosePurchaseRule()
        {
            BuyPosition = BuyPosition.NotPurchased;
        }
        public override TradePoint CriteriaForAction(ref ClusteringStructure sl, int bar, double currentClose)
        {
            TradePoint point = new TradePoint();
            if (sl.NextZone == BuyPosition.NotPurchased &&
                bar <= sl.Cluster.Right && bar >= sl.Cluster.Left)
            {
                if(CriteriaForAction(ref sl, currentClose))
                {
                    sl.NextZone = BuyPosition.NonTrendProfit;
                    point.Price = currentClose;
                }
            }
            return point;
        }
        public abstract bool CriteriaForAction(ref ClusteringStructure sl, double currentClose);
    }
    class ClusterTopPurchaseRule : ClusterClosePurchaseRule
    {
        public override bool CriteriaForAction(ref ClusteringStructure sl, double currentClose)
        {
            if (sl.ShortSale)
                return sl.Cluster.Bottom < currentClose;
            else
 	            return sl.Cluster.Top < currentClose;
        }
    }
    abstract class NonProfitSaleRule : SaleRule
    {
        public NonProfitSaleRule()
        {
            SellPosition = BuyPosition.NonTrendProfit;
        }
        public override TradePoint CriteriaForAction(ref ClusteringStructure sl, double TickSize)
        {
            TradePoint point = new TradePoint();
            sl.NextZone = BuyPosition.ADProfitZone;
            point.Price = CriteriaForActionInternal(ref sl, TickSize);
            point.Rule = this;
            return point;
        }
        protected abstract double CriteriaForActionInternal(ref ClusteringStructure sl, double TickSize);
    }
    class NonProfitLowSaleRule : NonProfitSaleRule
    {
        public NonProfitLowSaleRule(double salePercent = 0.66)
        {
            SalePercent = salePercent;
        }
        protected override double CriteriaForActionInternal(ref ClusteringStructure sl, double TickSize)
        {
            double cval = sl.C.GetAssociatedSwingValue(),
                    aval = sl.A.GetAssociatedSwingValue();
            return cval + (aval - cval) / 2;
        }
    }
    class NonProfitMidSaleRule : NonProfitSaleRule
    {
        public NonProfitMidSaleRule(double salePercent = 0.34)
        {
            SalePercent = salePercent;
        }
        protected override double CriteriaForActionInternal(ref ClusteringStructure sl, double TickSize)
        {
            double cval = sl.C.GetAssociatedSwingValue(),
                    aval = sl.A.GetAssociatedSwingValue();
            return  cval + (aval - cval) / 2;
        }
    }
    abstract class ADProfitSaleRule : SaleRule
    {
        public static double LowerFibBound { get; set; }
        public static double UpperFibBound { get; set; }
        public ADProfitSaleRule()
        {
            SellPosition = BuyPosition.ADProfitZone;
        }
        public override TradePoint CriteriaForAction(ref ClusteringStructure sl, double TickSize)
        {
            TradePoint point = new TradePoint();
            sl.NextZone = Base_Classes.BuyPosition.FinalProfitZone;
            point.Price = CriteriaForActionInternal(ref sl, TickSize);
            point.Rule = this;
            return point;
        }
        protected abstract double CriteriaForActionInternal(ref ClusteringStructure sl, double TickSize);
    }
    class ADProfitMidSaleRule : ADProfitSaleRule
    {
        public ADProfitMidSaleRule(double salePercent = 0.34)
        {
            SalePercent = salePercent;
        }
        protected override double CriteriaForActionInternal(ref ClusteringStructure sl, double TickSize)
        {
            SwingEntry D = new SwingEntry()
            {
                BarData = sl.D.BarData,
                BarLocation = sl.D.BarLocation,
                SwingType = sl.A.SwingType == SwingState.LowSwing ? SwingState.HighSwing : SwingState.LowSwing
            };
            double lower = D.FibonacciRetracements(sl.A, LowerFibBound),
                    upper = D.FibonacciRetracements(sl.A, UpperFibBound);

            if (sl.ShortSale)
                return lower + (upper - lower) / 2;
            else
                return lower - (upper - lower) / 2;
        }
    }
    class StopLossComparitor : StopLossRule
    {
        public override double StopLoss(ref ClusteringStructure sl, double sale_price)
        {
            if(sl.ShortSale)
                return sale_price + (sl.Cluster.Top - sl.D.BarData.Close) * 2;
            else
                return sale_price - (sl.D.BarData.Close - sl.Cluster.Bottom) * 2;
        }
    }
}
