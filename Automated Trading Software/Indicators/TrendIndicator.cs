﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Utilities;
using Automated_Trading_Software.Base_Classes;

namespace Automated_Trading_Software.Indicators
{
    public class TrendIndicator : Indicator
    {
        private SwingTracker m_tracker = null;
        private int m_count = 0;

        public int TrendStart { get; private set; }
        public String Name { get; set; }
        public TrendDirection CurrentTrend { get; private set; }

        public TrendIndicator(ref SwingTracker track)
        {
            DLLInit.Initialize();
            m_tracker = track;
        }

        public void Begin()
        {
        }

        public void BarUpdate(BarValue<double> CurrentBar)
        {
            SwingEntry lastlow = m_tracker.LastLow(Name),
                        lasthigh2 = m_tracker.LastHigh(Name, 2),
                        lasthigh = m_tracker.LastHigh(Name),
                        lastlow2 = m_tracker.LastLow(Name, 2);
            if (lastlow != null && lasthigh2 != null && lastlow.BarData.Low > lasthigh2.BarData.High)
            {
                TrendStart = lasthigh2.BarLocation;
                CurrentTrend = TrendDirection.Up;
            }
            else if (lasthigh != null && lastlow2 != null && lasthigh.BarData.High < lastlow2.BarData.Low)
            {
                TrendStart = lastlow2.BarLocation;
                CurrentTrend = TrendDirection.Down;
            }
            else
                CurrentTrend = TrendDirection.NoTrend;
            m_count++;
        }

        public void Terminate()
        {
        }
    }
}
