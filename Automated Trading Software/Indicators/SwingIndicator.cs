﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Utilities;
using System.IO;
using Automated_Trading_Software.Base_Classes;

namespace Automated_Trading_Software.Indicators
{
    public class SwingIndicator : Indicator, Observable<String, SwingEntry>, Observable<SwingState>
    {
        private int m_strength = 5;
        public int Strength
        { 
            get { return m_strength; } 
            set 
            {
                HiLoBuffer = new RollingCollection<BarValue<double>>(value, DefaultBar);
                m_strength = value; 
            } 
        }

        private string m_name;

        private RollingCollection<BarValue<double>> HiLoBuffer = null;
        private BarValue<double> LastBar = null;
        private int BarCount = 0;

        private SwingEntry runningEntry = new SwingEntry();
        private SwingEntry lastConfirm = new SwingEntry();

        private ObservedClass<String, SwingEntry> observable = new ObservedClass<String, SwingEntry>();
        private ObservedClass<SwingState> state_observable = new ObservedClass<SwingState>();

        public SwingState CurrentState { get; set; }
        public SwingEntry LastConfirmedEntry { get { return lastConfirm; } }
        public int CurrentBarCount { get { return BarCount; } }
        public bool EngulfingBar { get; set; }

        BarValue<double> DefaultBar()
        {
            return new BarValue<double>() { High = double.MinValue, Low = double.MaxValue };
        }

        public SwingIndicator(string instrument)
        {
            DLLInit.Initialize();
            m_name = instrument;
        }
        public void Begin()
        {
            if(HiLoBuffer == null)
                HiLoBuffer = new RollingCollection<BarValue<double>>(Strength, DefaultBar);
            LastBar = new BarValue<double>() { High = double.MinValue, Low = double.MaxValue };
            CurrentState = SwingState.BeforeCount;
            runningEntry.SwingType = SwingState.BeforeCount;
            runningEntry.BarData = new BarValue<double>() { High = double.MinValue, Low = double.MaxValue };
        }
        //There will obviously have to be a max tolerance here so you don't index data from the past year.
        public void BarUpdate(BarValue<double> CurrentBar)
        {
            SwingEntry entry = new SwingEntry()
            {
                BarData = CurrentBar,
                BarLocation = BarCount
            };

            //Is the bar inside? Skip
            if (CurrentBar.High > LastBar.High || CurrentBar.Low < LastBar.Low)
            {
                BarValue<bool> hl = HighLowAnalysis(CurrentBar, HiLoBuffer);
                if (BarCount >= Strength)
                {
                    EngulfingBar = hl.High && hl.Low;
                    //Is this the highest in the last strength of highs && lowest low in last strength of lows? Engulfing
                    if (hl.High ^ hl.Low) //&& we have completed a strength
                    {
                        EngulfingBar = false;
                        SwingState Previous = CurrentState;
                        if (hl.High)
                        {
                            //Is it lowest in the last strength of lows && are we currently in a high? Swing Change
                            CurrentState = SwingState.HighSwing;
                        }
                        else
                        //Is this the highest in the last strength of lows? Low
                        if (hl.Low)
                        {
                            //Is it highest in the last strength of hights && are we currently in a low? Swing Change
                            CurrentState = SwingState.LowSwing;
                        }

                        if (Previous != CurrentState)
                        {
                            lastConfirm.BarData = entry.BarData;
                            lastConfirm.BarLocation = entry.BarLocation;
                            lastConfirm.SwingType = CurrentState;
                            if (runningEntry.SwingType != SwingState.BeforeCount)
                            {
                                notifyObservers(runningEntry.Clone());
                                runningEntry.BarData = DefaultBar();
                                runningEntry.BarLocation = 0;
                                runningEntry.SwingType = CurrentState;
                            }
                        }
                    }
                }

                if ((!EngulfingBar && BarCount >= Strength) || BarCount < Strength)
                {
                    HiLoBuffer[HiLoBuffer.NextIndex].High = CurrentBar.High;
                    HiLoBuffer[HiLoBuffer.NextIndex].Low = CurrentBar.Low;
                    HiLoBuffer.NextIndex++;
                    LastBar = new BarValue<double>(CurrentBar);
                }
            }

            if ((CurrentState == SwingState.HighSwing && runningEntry.BarData.High <= entry.BarData.High) ||
                (CurrentState == SwingState.LowSwing && runningEntry.BarData.Low >= entry.BarData.Low))
            {
                entry.SwingType = CurrentState;
                runningEntry = entry;
            }

            notifyObservers(CurrentState);
            BarCount++;
        }
        public void Terminate()
        {
        }
        private BarValue<bool> HighLowAnalysis(BarValue<double> value, RollingCollection<BarValue<double>> coll)
        {
            BarValue<bool> result = new BarValue<bool>() { High = true, Low = true };
            for (int i = 0; i < Strength; ++i)
            {
                result.High &= value.High > coll[i].High;
                result.Low &= value.Low < coll[i].Low;
            }
            return result;
        }
        public void addObserver(Action<String, SwingEntry> entry)
        {
            observable.addObserver(entry);
        }
        private void notifyObservers(SwingEntry entry)
        {
            observable.notifyObservers(m_name, entry);
        }
        public void addObserver(Action<SwingState> entry)
        {
            state_observable.addObserver(entry);
        }
        private void notifyObservers(SwingState entry)
        {
            state_observable.notifyObservers(entry);
        }
    }
}
