﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Base_Classes;
using Automated_Trading_Software.Strategies;

namespace Automated_Trading_Software.Risk
{
    public class MarketKnowledge
    {
        public DateTime TimeGiven { get; set; }
        public DateTime Expiry { get; set; }

        public TimeSpan Resolution { get; set; }
        public string SecurityName { get; set; }

        public int Sector { get; set; }
        public double MarketTrend { get; set; }
    }
    public interface RiskAnalyser
    {
        void AddKnowledge(MarketKnowledge knowledge);
        void TradedUnits(BuyPosition zone, string secName, TimeSpan resolution, int currentunits);
        PurchasingRuleSet AssessStrategy(String instrument, double tickSize, double confidence, int sector);
    }
}
