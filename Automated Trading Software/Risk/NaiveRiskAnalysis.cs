﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Base_Classes;
using Automated_Trading_Software.Strategies;

namespace Automated_Trading_Software.Risk
{
    class NaiveRiskAnalysis : RiskAnalyser
    {
        private List<MarketKnowledge> m_data = new List<MarketKnowledge>();

        public void AddKnowledge(MarketKnowledge knowledge)
        {
            m_data.Add(knowledge);
            m_data.RemoveAll(x => x.TimeGiven < DateTime.Now.Subtract(TimeSpan.FromHours(24)));
        }
        public PurchasingRuleSet AssessStrategy(String instrument, double barSize, double confidence, int sector)
        {
            PurchasingRuleSet rules = PurchasingRuleSet.GetMediumInvestmentRuleSet();
            return rules;

            int high = 0, low = 0;
            foreach (MarketKnowledge knowledge in m_data)
            {
                if (knowledge.SecurityName == instrument && DateTime.Now < knowledge.Expiry)
                {
                    if (knowledge.TimeGiven > DateTime.Now)
                    {
                        if (knowledge.MarketTrend == -1)
                        {
                            return null;
                        }
                        else if (knowledge.MarketTrend == -2)
                        {
                            return PurchasingRuleSet.GetSimpleInvestmentRuleSet();
                        }
                        else if (knowledge.MarketTrend == 1)
                        {
                            return PurchasingRuleSet.GetHighInvestmentRuleSet();
                        }
                    }

                    if (knowledge.Resolution.TotalMinutes > barSize)
                    {
                        high += 5;
                    }
                    else if (knowledge.MarketTrend > 0)
                    {
                        high += 5;
                    }
                    else if (knowledge.MarketTrend < 0)
                    {
                        low += 5;
                    }
                }

                if (knowledge.Sector == sector)
                {
                    if (knowledge.MarketTrend > 0)
                    {
                        high++;
                    }
                    else if (knowledge.MarketTrend < 0)
                    {
                        low++;
                    }
                }
            }
            if (high > 0 || low > 0)
            {
                if (high > low)
                {
                    return PurchasingRuleSet.GetHighInvestmentRuleSet();
                }
                else
                {
                    return PurchasingRuleSet.GetLowInvestmentRuleSet();
                }
            }
            return rules;
        }
        public void TradedUnits(BuyPosition zone, string secName, TimeSpan resolution, int currentunits)
        {
        }
    }
}
