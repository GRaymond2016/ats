﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Base_Classes;

namespace Automated_Trading_Software.Utilities
{
    public class SwingTracker
    {
        private int first = 0;
        private RollingInt accessor = new RollingInt(1000);
        private Dictionary<String, RollingCollection<SwingEntry>> m_internalCatalogue = new Dictionary<String, RollingCollection<SwingEntry>>();
        private object m_internalCatalogueLock = new object();

        public SwingState CurrentTrend { get; private set; }

        public SwingTracker()
        {
            CurrentTrend = SwingState.BeforeCount;
        }
        public void SetCurrentState(SwingState state)
        {
            CurrentTrend = state;
        }
        public void AddSwing(String instrumentName, SwingEntry entry)
        {
            lock (m_internalCatalogueLock)
            {
                if (!m_internalCatalogue.ContainsKey(instrumentName))
                    m_internalCatalogue[instrumentName] = new RollingCollection<SwingEntry>(1000, Activator.CreateInstance<SwingEntry>);

                //Do not allow duplicate entries for a specific BarLocation.
                for (RollingInt i = new RollingInt(accessor); ; --i)
                {
                    if (i.Value == first)
                        break;
                }
                m_internalCatalogue[instrumentName][accessor] = entry;
                accessor++;
                if (accessor.Rolled == true)
                    first = (int)(accessor.Value + 1 >= accessor.Max ? 0 : accessor.Value + 1);
            }
        }
        public SwingEntry LastLow(String instrumentName, int ago = 1)
        {
            return LastAgo(instrumentName, x => x.SwingType == SwingState.LowSwing, ago);
        }
        public SwingEntry LastHigh(String instrumentName, int ago = 1)
        {
            return LastAgo(instrumentName, x => x.SwingType == SwingState.HighSwing, ago);
        }
        public SwingEntry LastAgo(String instrumentName, Predicate<SwingEntry> cond, int ago = 1)
        {
            lock (m_internalCatalogueLock)
            {
                if (!m_internalCatalogue.ContainsKey(instrumentName))
                    return Activator.CreateInstance<SwingEntry>();
                if (ago <= 0)
                    throw new ArgumentException();
                for (RollingInt i = new RollingInt(accessor); ; --i)
                {
                    if (cond(m_internalCatalogue[instrumentName][i]) && --ago == 0)
                        return m_internalCatalogue[instrumentName][i];
                    if (i.Value == first)
                        return Activator.CreateInstance<SwingEntry>();
                }
            }
        }
        public SwingEntry LastAgo(String instrumentName, int ago = 1)
        {
            lock (m_internalCatalogueLock)
            {
                if (!m_internalCatalogue.ContainsKey(instrumentName))
                    return Activator.CreateInstance<SwingEntry>();
                if (ago <= 0)
                    throw new ArgumentException();
                for (RollingInt i = new RollingInt(accessor); ; --i)
                {
                    if (--ago == -1)
                        return m_internalCatalogue[instrumentName][i];
                }
            }
        }
        public List<SwingEntry> GiveMeAllPoints(String instrumentName)
        {
            lock (m_internalCatalogueLock)
            {
                //This is probably quite inefficient, it will search through all elements and unless
                //the compiler is smart do two copies.
                return m_internalCatalogue[instrumentName].Data.Where(x => x.SwingType != SwingState.BeforeCount).ToList();
            }
        }
    }
}
