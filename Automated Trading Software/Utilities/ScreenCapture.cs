﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace Automated_Trading_Software.Utilities
{
    public class ScreenCapture
    {
        [DllImport("user32.dll")]
        static extern IntPtr GetActiveWindow();

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool GetWindowRect(HandleRef hWnd, out RECT lpRect);

        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            public int Left;        // x position of upper-left corner
            public int Top;         // y position of upper-left corner
            public int Right;       // x position of lower-right corner
            public int Bottom;      // y position of lower-right corner
        }

        public void CaptureActiveScreen()
        {
            IntPtr handle = IntPtr.Zero;
            handle = GetActiveWindow();
            if (handle == IntPtr.Zero)
                handle = GetForegroundWindow();
            RECT rc;
            GetWindowRect(new HandleRef(this, handle), out rc);

            Size sz = new Size(rc.Right - rc.Left, rc.Bottom - rc.Top);

            if (sz.Height == 0 || sz.Width == 0)
                return;

            Bitmap shot = new Bitmap(sz.Width, sz.Height);
            Graphics gs = Graphics.FromImage(shot);
            Point upleft = new Point(rc.Left, rc.Top);
            gs.CopyFromScreen(upleft, new Point(0, 0), sz);

            string userPath = Environment.GetEnvironmentVariable("USERPROFILE");
            string screenshot = userPath + "\\Documents\\Cluster Zone Screenshots\\";
            if (!Directory.Exists(screenshot))
                Directory.CreateDirectory(screenshot);

            screenshot += DateTime.Now.ToShortDateString().Replace("/", ".") + " " + DateTime.Now.ToShortTimeString().Replace(":", ".") + ".bmp";
            shot.Save(screenshot, ImageFormat.Bmp);
        }
    }
}
