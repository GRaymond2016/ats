﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.CompilerServices;

namespace Automated_Trading_Software.Utilities
{
    //To allow easy wrapping of an array
    public class RollingInt
    {
        //Pretty much a hack way of doing this. Needs more thought.
        private bool Init = false;

        public bool Rolled { get { return Init && Value == 0; } }
        public int Max { private set; get; }

        public int Value = 0;

        public RollingInt(int max)
        {
            Max = max;
        }
        public RollingInt(RollingInt reference)
        {
            Max = reference.Max;
            Init = reference.Init;
            Value = reference.Value;
        }
        public int VerifyLimits()
        {
            Init = true;
            int lr = Value % Max;
            return lr < 0 ? lr + Max : lr;
        }
        public static implicit operator RollingInt(int rhs)
        {
            return new RollingInt(int.MaxValue)
            {
                Value = rhs
            };
        }
        public static implicit operator int(RollingInt rhs)
        {
            return rhs.Value;
        }
        public static RollingInt operator++(RollingInt rhs)
        {
            ++rhs.Value;
            rhs.Value = rhs.VerifyLimits();
            return rhs;
        }
        public static RollingInt operator--(RollingInt rhs)
        {
            rhs.Value--;
            rhs.Value = rhs.VerifyLimits();
            return rhs;
        }
        public static RollingInt operator +(RollingInt rhs, RollingInt lhs)
        {
            RollingInt temp = rhs;
            temp.Value += lhs.Value;
            temp.Value = temp.VerifyLimits();
            return temp;
        }
        public static RollingInt operator -(RollingInt rhs, RollingInt lhs)
        {
            RollingInt temp = rhs;
            temp.Value -= lhs.Value;
            temp.Value = temp.VerifyLimits();
            return temp;
        }
    }
    //Simple access to a fixed sized array
    public class RollingCollection<T>
    {
        public RollingInt NextIndex;
        public T[] Data;
        public RollingCollection(int count, Func<T> defaultValue)
        {
            Data = new T[count];
            for (int i = 0; i < count; ++i)
                Data[i] = defaultValue();
            NextIndex = new RollingInt(count);
        }
        //Just for ease of access to the array
        public T this[RollingInt i]
        { get { return this[i.Value]; } set { this[i.Value] = value; } }
        public T this[int i]
        { get { return Data[i]; } set { Data[i] = value; } }
    }
}
