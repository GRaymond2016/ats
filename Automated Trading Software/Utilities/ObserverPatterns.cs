﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automated_Trading_Software.Utilities
{
    public interface Observable<T>
    {
        void addObserver(Action<T> entry);
    }
    public interface Observable<T, A>
    {
        void addObserver(Action<T, A> entry);
    }
    public class ObservedClass<T>
    {
        private List<Action<T>> m_observers = new List<Action<T>>();

        public void addObserver(Action<T> entry)
        {
            m_observers.Add(entry);
        }
        public void notifyObservers(T entry)
        {
            foreach (var onChange in m_observers)
                onChange(entry);
        }
    }
    public class ObservedClass<T, A>
    {
        private List<Action<T,A>> m_observers = new List<Action<T,A>>();

        public void addObserver(Action<T,A> entry)
        {
            m_observers.Add(entry);
        }
        public void notifyObservers(T key, A entry)
        {
            foreach (var onChange in m_observers)
                onChange(key, entry);
        }
    }
    public class StaticallyObservedClass<T>
    {
        //For brevity, could revise this with a hash but scared of complexity.
        private static Dictionary<Type, List<Action<T>>> m_observers = new Dictionary<Type, List<Action<T>>>();
        private static object m_observer_lock = new object();

        public static void addObserver(Type context, Action<T> entry)
        {
            lock (m_observer_lock)
            {
                if (!m_observers.ContainsKey(context))
                    m_observers.Add(context, new List<Action<T>>());
                m_observers[context].Add(entry);
            }
        }
        public static void notifyObservers(Type context, T entry)
        {
            lock (m_observer_lock)
            {
                foreach (var onChange in m_observers[context])
                    onChange(entry);
            }
        }
    }
}
