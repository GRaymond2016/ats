﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using Automated_Trading_Software.Risk;
using System.Collections.Generic;
using Automated_Trading_Software.Base_Classes;

namespace Automated_Trading_Software.Utilities
{

    public interface RiskManager
    {
        Func<double> FundRetrieval { set; }
        int Risk { set; }
        int UnitsToTrade(RiskAnalyser risk, double bid);
    }
    public class FixedFractionalRiskManager : StaticallyObservedClass<KeyValuePair<string, string>>, RiskManager 
    {
        public Func<double> FundRetrieval
        {
            private get;
            set;
        }

        public int Risk { set { RiskPercentage = value; } }
        private int RiskPercentage = 1;
        public const string FundsKey = "Spent";

        public int UnitsToTrade(RiskAnalyser risk, double bid)
        {
            if (FundRetrieval == null)
                throw new InvalidOperationException("Could not retrieve fund count at this time.");

            double funds = FundRetrieval(); 
            double percent = (0.01 * (double) RiskPercentage);

            int return_value = (int)Math.Floor((funds * percent) / bid);
            if (return_value == 0)
                notifyObservers(this.GetType(), new KeyValuePair<string, string>("Error", "UnitsToBuy returned a zero value, this may cause loss of sale."));
            return return_value;
        }
    }
}
