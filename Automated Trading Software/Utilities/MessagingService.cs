﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Pipes;
using System.Threading;
using System.IO;

namespace Automated_Trading_Software.Utilities
{
    public class MessagingService
    {
        private static Thread ts = null;
        private static bool keep_running = true;
        private static NamedPipeClientStream client = new NamedPipeClientStream("ATS.Base.NaiveFinancialAdvisor.DataStream");
        private static object tracking_lock = new object();
        private static List<string> sales_tracking = new List<string>();
        private static object type_lock = new object();
        private static Dictionary<string, MessageType> message_types = new Dictionary<string, MessageType>();
        private const string delim = ":";

        static MessagingService()
        {
            //The thread start has been removed as this interface is not really necessary, it just means more reliance on NinjaTrader.
        }
        ~MessagingService()
        {
            keep_running = false;
            ts.Join();
            client.Close();
        }
        private static void WindowLaunch()
        {
            client.Connect();

            while (keep_running)
            {
                try
                {
                    if (!client.IsConnected)
                    {
                        client = new NamedPipeClientStream("ATS.Base.NaiveFinancialAdvisor.DataStream");
                        client.Connect();
                    }

                    using (StreamWriter sw = new StreamWriter(client))
                    {
                        lock (tracking_lock)
                        {
                            foreach(string v in sales_tracking)
                            {
                                sw.WriteLine(v);
                            }
                            sales_tracking.Clear();
                        }
                        
                    }
                }
                catch
                {
                    //Exception Occurred, but no one to tell about it.
                }
                Thread.Sleep(100);
            }
        }
        public static void AddMessageKey(string key, MessageType type)
        {
            lock (type_lock)
            {
                message_types.Add(key, type);
            }
        }
        public static void TranslateMessage(KeyValuePair<string, string> msg)
        {
            MessageType type = MessageType.RunningFeed;
            lock (type_lock)
            {
                if(message_types.ContainsKey(msg.Key))
                    type = message_types[msg.Key];
            }
            AddMessage(new ATSInternalMessage() { type = type, key = msg.Key, value = msg.Value });
        }
        public static void AddMessage(ATSInternalMessage msg)
        {
            lock (tracking_lock)
            {
                string emal = "";
                switch (msg.type)
                {
                    case MessageType.Constant: emal += "C"; break;
                    case MessageType.RunningFeed: emal += "R"; break;
                    default: return;
                }
                emal += delim + msg.key + delim + msg.value;
                sales_tracking.Add(emal);
            }
        }
    }
}
