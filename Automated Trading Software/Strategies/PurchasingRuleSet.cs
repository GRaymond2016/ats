﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Indicators;

namespace Automated_Trading_Software.Strategies
{
    public class PurchasingRuleSet
    {
        public PurchasingRuleSet()
        {
            PurchasingRules = new List<PurchaseRule>();
            SellingRules = new List<SaleRule>();
        }
        public List<PurchaseRule> PurchasingRules { get; set; }
        public List<SaleRule> SellingRules { get; set; }
        public StopLossRule StopLossRule { get; set; }

        static public PurchasingRuleSet GetHighInvestmentRuleSet()
        {
            PurchasingRuleSet ruleset = new PurchasingRuleSet();
            ruleset.PurchasingRules.Add(new ClusterTopPurchaseRule());
            ruleset.SellingRules.Add(new ADProfitMidSaleRule());
            ruleset.StopLossRule = new StopLossComparitor();
            return ruleset;
        }
        static public PurchasingRuleSet GetMediumInvestmentRuleSet()
        {
            PurchasingRuleSet ruleset = new PurchasingRuleSet();
            ruleset.PurchasingRules.Add(new ClusterTopPurchaseRule());
            ruleset.SellingRules.Add(new NonProfitMidSaleRule());
            ruleset.SellingRules.Add(new ADProfitMidSaleRule());
            ruleset.StopLossRule = new StopLossComparitor();
            return ruleset;
        }
        static public PurchasingRuleSet GetLowInvestmentRuleSet()
        {
            PurchasingRuleSet ruleset = new PurchasingRuleSet();
            ruleset.PurchasingRules.Add(new ClusterTopPurchaseRule());
            ruleset.SellingRules.Add(new NonProfitLowSaleRule());
            ruleset.SellingRules.Add(new ADProfitMidSaleRule());
            ruleset.StopLossRule = new StopLossComparitor();
            return ruleset;
        }
        static public PurchasingRuleSet GetSimpleInvestmentRuleSet()
        {
            PurchasingRuleSet ruleset = new PurchasingRuleSet();
            ruleset.PurchasingRules.Add(new ClusterTopPurchaseRule());
            ruleset.SellingRules.Add(new NonProfitLowSaleRule(1.0));
            ruleset.StopLossRule = new StopLossComparitor();
            return ruleset;
        }
    }
}
