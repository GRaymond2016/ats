﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using Automated_Trading_Software.Base_Classes;
using Automated_Trading_Software.Utilities;
using Automated_Trading_Software.Indicators;

namespace Automated_Trading_Software.Strategies
{
    public class PurchaseTracker : StaticallyObservedClass<KeyValuePair<string,string>>
    {
        private static object advisor_lock = new object();
        private static RiskManager advise = null;
        public static RiskManager Advisor 
        {
            get
            {
                lock (advisor_lock)
                {
                    return advise;
                }
            }
            set
            {
                lock (advisor_lock)
                {
                    advise = value;
                }
            }
        }

        private static List<TradePoint> purchases = new List<TradePoint>();
        private static object purch_lock = new object();

        private const string BuyKey = "Buy";
        private const string SellKey = "Sell";

        public static void AdviseUnitsToPurchase(Risk.RiskAnalyser risk, ref TradePoint order)
        {
            order.Units = Advisor.UnitsToTrade(risk, order.Price);
        }
        public static void AdviseUnitsToSell(ref TradePoint order)
        {
            order.Units = (int) Math.Floor(order.Rule.SalePercent * order.Units);
            if(order.Units == 0)
                notifyObservers(typeof(PurchaseTracker), new KeyValuePair<string, string>("Error", "UnitsToBuy returned a zero value, this may cause loss of sale."));
        }
        public static void ConfirmPurchase(ClusteringIndicator indicator, TradePoint order, string name)
        {
            //Placeholder for tracking
        }
        public static void ConfirmSale(ClusteringIndicator indicator, TradePoint order, string name)
        {
            indicator.RunFunctorOnAnalyser(delegate(Risk.RiskAnalyser a){ a.TradedUnits(order.Rule.SellPosition, indicator.InstrumentName, TimeSpan.FromMinutes(indicator.BarResolution), order.Units); });
            notifyObservers(typeof(PurchaseTracker), new KeyValuePair<string, string>(SellKey, order.Units.ToString() + " units at $" + order.Price.ToString()));
        }
    }
}
