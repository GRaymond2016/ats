﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Utilities;

namespace Automated_Trading_Software.Base_Classes
{
    //This is the four values in a bar defined as any data type
    public class BarValue<T>
    {
        public BarValue()
        {
        }
        public BarValue(BarValue<T> init)
        {
            High = init.High;
            Low = init.Low;
            Open = init.Open;
            Close = init.Close;
        }
        public T High;
        public T Low;
        public T Open;
        public T Close;

        public override bool Equals(object obj)
        {
            if (!(obj is BarValue<T>) || obj == null) return false;

            BarValue<T> compare = (BarValue<T>)obj;
            return base.Equals(obj) && High.Equals(compare.High) && Low.Equals(compare.Low) && Open.Equals(compare.Open) && Close.Equals(compare.Close);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
    public class BarEntry : IComparable
    {
        public int BarLocation { get; set; }
        public BarValue<double> BarData { get; set; }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            return Comparison(this, (BarEntry)obj);
        }
        public static int Comparison(BarEntry entry1, BarEntry entry2)
        {
            if (entry1 < entry2)
                return -1;
            if (entry1 > entry2)
                return 1;

            return 0;
        }
        public static bool operator <(BarEntry entry1, BarEntry entry2)
        {
            return entry1.BarLocation < entry2.BarLocation;
        }
        public static bool operator >(BarEntry entry1, BarEntry entry2)
        {
            return entry1.BarLocation > entry2.BarLocation;
        }
        public override bool Equals(object obj)
        {
            if (!(obj is BarEntry) || obj == null) return false;

            BarEntry compare = (BarEntry)obj;
            return this.BarLocation == compare.BarLocation && this.BarData.Equals(compare.BarData);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion
    }
}
