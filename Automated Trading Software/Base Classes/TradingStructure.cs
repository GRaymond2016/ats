﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Indicators;

namespace Automated_Trading_Software.Base_Classes
{
    public class TradePoint
    {
        public String Instrument;
        public TimeSpan Timeframe;
        public double MaxPrice;
        public double Price;
        public int Units;
        public double StopLoss;
        public SaleRule Rule;
        public double ClusterEnd;
        public bool ShortSale;
    }
    public class TradingStructure
    {
        public DateTime lastTradePoint;
        public int RecoveredUnits;
        public TradePoint Purchase;
        public Dictionary<BuyPosition, TradePoint> SalePoints = new Dictionary<BuyPosition,TradePoint>();
    }
}
