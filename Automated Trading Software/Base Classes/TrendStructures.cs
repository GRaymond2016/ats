﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automated_Trading_Software.Base_Classes
{
    public enum TrendDirection
    {
        Up,
        Down,
        NoTrend
    }
    public class Rectangle<T> where T : IComparable
    {
        public T Top { get; set; }
        public T Bottom { get; set; }
        public T Left { get; set; }
        public T Right { get; set; }
        public bool Hit(T x, T y)
        {
            return y.CompareTo(Top) < 0 && y.CompareTo(Bottom) > 0 && x.CompareTo(Left) > 0 && x.CompareTo(Right) < 0;
        }
    }
}
