﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Automated_Trading_Software.Strategies;
using Automated_Trading_Software.Utilities;
using Automated_Trading_Software.Indicators;

namespace Automated_Trading_Software.Base_Classes
{
    static class DLLInit
    {
        static DLLInit()
        {
            //This decouples purchaser and NFA from the messaging service and allows us to add more observers without adding dependancies
            MessagingService.AddMessageKey(FixedFractionalRiskManager.FundsKey, MessageType.Constant);
            PurchaseTracker.addObserver(typeof(PurchaseTracker), MessagingService.TranslateMessage);
            FixedFractionalRiskManager.addObserver(typeof(FixedFractionalRiskManager), MessagingService.TranslateMessage);
            PurchaseTracker.Advisor = new FixedFractionalRiskManager();

            ClusteringIndicator.ClusterHigh = 121;
            ClusteringIndicator.ClusterLow = 162;
            ADProfitSaleRule.LowerFibBound = 121;
            ADProfitSaleRule.UpperFibBound = 162;
        }
        public static void Initialize()
        {
        }
    }
}
