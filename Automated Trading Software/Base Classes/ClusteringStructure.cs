﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automated_Trading_Software.Base_Classes
{
    public enum BuyPosition
    {
        NotPurchased,
        NonTrendProfit,
        ADProfitZone,
        FinalProfitZone
    }
    public class ClusteringStructure
    {
        public ClusteringStructure()
        {
            Purchased = Unpurchased;
            NextZone = BuyPosition.NotPurchased;
            ClusterHits = 0;
        }

        public const long Unpurchased = 0;
        public const long Invalidated = -1;
        public const long Complete = -1;

        //Placeholder for a risk assessment of the structure.
        public double MathematicalPrecision { get { return 1; } }

        public bool ShortSale { get { return X.BarData.Low > A.BarData.High; } }

        public SwingEntry Origin { get; set; }
        public SwingEntry X { get; set; }
        public SwingEntry A { get; set; }
        public SwingEntry B { get; set; }
        public SwingEntry C { get; set; }
        public BarEntry D { get; set; }
        public Rectangle<double> Cluster { get; set; }
        public int ClusterHits { get; set; }
        public long Purchased { get; set; }
        public double StopLoss { get; set; }
        public BuyPosition NextZone { get; set; }

        public double AXLowRetracement { get; set; }
        public double AXHighRetracement { get; set; }
        public double ClusterMax { get; set; }
        public double ClusterMin { get; set; }
        public double Equality { get; set; }
    }
}
