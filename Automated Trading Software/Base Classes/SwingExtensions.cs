﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automated_Trading_Software.Base_Classes
{
    public static class SwingExtensions
    {
        public static bool Valid(this SwingEntry entry)
        {
            return entry.SwingType != SwingState.BeforeCount;
        }
        public static double GetAssociatedSwingValue(this SwingEntry entry)
        {
            if (entry.SwingType == SwingState.HighSwing)
                return entry.BarData.High;
            else
                return entry.BarData.Low;
        }
        public static double DistanceTo(this SwingEntry entry, SwingEntry otherEntry, double marker)
        {
            double a = entry.GetAssociatedSwingValue() / marker,
                   b = otherEntry.GetAssociatedSwingValue() / marker;

            a = Math.Abs(a - b);
            b = Math.Abs(entry.BarLocation - otherEntry.BarLocation);
            return Math.Sqrt(a * a + b * b);
        }
        public static double DistanceTo(this SwingEntry entry, double barlocation, double value, double marker)
        {
            double a = Math.Abs(GetAssociatedSwingValue(entry) / marker - value / marker),
                   b = Math.Abs(entry.BarLocation - barlocation);
            return Math.Sqrt(a * a + b * b);
        }
        public static double FibonacciRetracements(this SwingEntry entry, SwingEntry comp, double value)
        {
            if (entry.SwingType == SwingState.HighSwing)
                return entry.GetAssociatedSwingValue() - (entry.GetAssociatedSwingValue() - comp.GetAssociatedSwingValue()) * (value * 0.01);
            else
                return entry.GetAssociatedSwingValue() + (comp.GetAssociatedSwingValue() - entry.GetAssociatedSwingValue()) * (value * 0.01);
        }
    }
}
