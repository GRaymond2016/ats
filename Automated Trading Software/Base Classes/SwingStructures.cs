﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Automated_Trading_Software.Base_Classes
{
    public enum SwingState
    {
        BeforeCount,
        EngulfingSwing,
        HighSwing,
        LowSwing
    }
    public class SwingEntry : BarEntry, IComparable
    {
        public SwingState SwingType { get; set; }
        public SwingEntry Clone()
        {
            return new SwingEntry()
            {
                SwingType = SwingType,
                BarLocation = BarLocation,
                BarData = new BarValue<double>()
                {
                    Close = BarData.Close,
                    High = BarData.High,
                    Low = BarData.Low,
                    Open = BarData.Open
                }
            };
        }
        #region IComparable Members
        int IComparable.CompareTo(object obj)
        {
            return this.CompareTo(obj);
        }
        #endregion
    }
}
