﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.IO.Pipes;
using System.IO;

namespace ATS.TradingInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class KeyValuePair
        {
            public string Key { get; set; }
            public string Value { get; set; }
        }

        private Dictionary<string, NamedPipeServerStream> connections = new Dictionary<string, NamedPipeServerStream>();
        private Thread background;
        private bool active = true;

        public MainWindow()
        {
            InitializeComponent();
            background = new Thread(new ThreadStart(GetDataFromSources));
            background.Start();
        }
        ~MainWindow()
        {
            active = false;
            background.Join();
            foreach (var v in connections)
                v.Value.Close();
        }
        void GetDataFromSources()
        {
            while (active)
            {
                try
                {
                    PollSource("ATS.Base.NaiveFinancialAdvisor.DataStream");
                }
                catch(IOException)
                {
                    //IO exceptions may validly occur in this part of the code
                }
                Thread.Sleep(100);
            }
        }
        void PollSource(string name)
        {
            if (!connections.ContainsKey(name))
            {
                connections.Add(name, new NamedPipeServerStream(name));
                connections[name].WaitForConnection();
            }
            if (connections[name].IsConnected)
            {
                using (StreamReader rs = new StreamReader(connections[name]))
                {
                    while (!rs.EndOfStream)
                    {
                        string line = rs.ReadLine();
                        string[] result = line.Split(new char[] { ':' });
                        if (result.Length > 2)
                        {
                            switch (result[0])
                            {
                                case "C":
                                    Dispatcher.Invoke(new Action(delegate { UpdateConstant(result[1], result[2]); }));
                                    break;
                                case "R":
                                    Dispatcher.Invoke(new Action(delegate { AddKeyValuePair(result[1], result[2]); }));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
            }
            else
                connections.Remove(name);
        }
        public void UpdateConstant(string key, string value)
        {
            textBlock1.Visibility = System.Windows.Visibility.Hidden;
            foreach (var l in listView1.Items.OfType<KeyValuePair>())
            {
                if (l.Key == key)
                {
                    l.Value = value;
                    listView1.Items.Refresh();
                    return;
                }
            }
            listView1.Items.Add(new KeyValuePair { Key = key, Value = value });
            listView1.Items.Refresh();
        }
        public void AddKeyValuePair(string key, string value)
        {
            listView2.Items.Add(new KeyValuePair { Key = key, Value = value });
            listView2.Items.Refresh();
        }
    }

}
