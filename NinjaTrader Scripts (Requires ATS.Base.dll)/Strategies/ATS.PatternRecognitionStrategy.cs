/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Indicator;
using NinjaTrader.Gui.Chart;
using NinjaTrader.Strategy;
using ATSI = Automated_Trading_Software.Indicators;
using ATS = Automated_Trading_Software;
using ATSS = Automated_Trading_Software.Strategies;
#endregion

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    /// <summary>
    /// A market strategy for the buying and selling of market structures.
    /// </summary>
    [Description("A market strategy for the buying and selling of market structures.")]
    public class ATSPatternRecognitionStrategy : Strategy
    {
        #region Variables
        // Wizard generated variables
        private int strength = 5; // Default setting for Strength
        private int lowFibBound = 127; // Default setting for LowFibBound
        private int upperFibBound = 162; // Default setting for UpperFibBound
        // User defined variables (add any user defined variables below)
        #endregion

        #region Configurable Parameters
        [Description("Percentage of AB that the BC line must retrace for the structure to be valid.")]
        [Browsable(true)]
        [XmlIgnore]
        public double BCRatio
        {
            get { return m_bcratio; }
            set { m_bcratio = value; }
        }
        private double m_bcratio = 0.5;

        [Description("Percentage of BC that the CD line must retrace for the structure to be valid.")]
        [Browsable(true)]
        [XmlIgnore]
        public double CDRatio
        {
            get { return m_cdratio; }
            set { m_cdratio = value; }
        }
        private double m_cdratio = 0.5;

        [Description("Percentage of XA that the AB line must retrace for the structure to be valid.")]
        [Browsable(true)]
        [XmlIgnore]
        public double ABRatio
        {
            get { return m_abratio; }
            set { m_abratio = value; }
        }
        private double m_abratio = 0.1;

        [Description("Require that equality be in the cluster zone to recognize a structure.")]
        [Browsable(true)]
        [XmlIgnore]
        public bool RequireEquality
        {
            get { return m_requality; }
            set { m_requality = value; }
        }
        private bool m_requality;

        [Description("Require the cluster zone to be partially within 50 - 76.5 retracement of AX.")]
        [Browsable(true)]
        [XmlIgnore]
        public bool RequireClusterBounds
        {
            get { return m_clusterBounds; }
            set { m_clusterBounds = value; }
        }
        private bool m_clusterBounds;

        [Description("Specify the value fibonacci retracement of XA to be used as a higher bound for B to enter. (Default: 50)")]
        [Browsable(true)]
        [XmlIgnore()]
        public float FibABUpperBound
        {
            get { return m_ABfibUpper; }
            set { m_ABfibUpper = value; }
        }
        private float m_ABfibUpper = 50f;

        [Description("Specify the value fibonacci retracement of XA to be used as a lower bound for B to enter. (Default: 31.5)")]
        [Browsable(true)]
        [XmlIgnore()]
        public float FibABLowerBound
        {
            get { return m_ABfibLower; }
            set { m_ABfibLower = value; }
        }
        private float m_ABfibLower = 31.5f;

        [Description("The number of bars that must close within the cluster before a trade happens.")]
        [Browsable(true)]
        [XmlIgnore()]
        public int CloseCount
        {
            get { return m_closeCount; }
            set { m_closeCount = value; }
        }
        private int m_closeCount = 3;

        #endregion

        #region Strategy Indicators
        ATS.Utilities.SwingTracker trck = new ATS.Utilities.SwingTracker();
        ATSI.SwingIndicator swng;
        ATSI.ClusteringIndicator cls;
        #endregion

        #region Trade Management
        class TradeStructure
        {
            public ATS.Base_Classes.TradingStructure structure;
            public List<string> sale_names = new List<string>();
        }
        class RunningTrade
        {
            public bool shortbuy;
            public string name;
        }
        List<TradeStructure> trades = new List<TradeStructure>();
        List<RunningTrade> running_trades = new List<RunningTrade>();
        ulong count = 0;
        #endregion

        /// <summary>
        /// This method is used to configure the strategy and is called once before any strategy method is called.
        /// </summary>
        protected override void Initialize()
        {
            CalculateOnBarClose = true;
            swng = new ATSI.SwingIndicator(Instrument.FullName);

            swng.Begin();
            swng.Strength = strength;

            swng.addObserver(trck.AddSwing);
            swng.addObserver(trck.SetCurrentState);

            cls = new ATSI.ClusteringIndicator(ref trck, Instrument.FullName);
            cls.Begin();
            cls.addObserver(MakeTrade);
            cls.ABtoBCRatio = m_bcratio;
            cls.BCtoCDRatio = m_cdratio;
            cls.XAtoABRatio = m_abratio;
            cls.RequireEquality = m_requality;
            cls.RequireClusterBounds = m_clusterBounds;
            cls.CloseCount = CloseCount;
            cls.ABFibHighRetracement = FibABUpperBound;
            cls.ABFibLowRetracement = FibABLowerBound;

            ATSS.PurchaseTracker.Advisor.FundRetrieval = GetFunds;
            ATSS.PurchaseTracker.Advisor.Risk = 1;
        }

        private double GetFunds()
        {
            //Because I hate accounts.
            return 510000;
        }

        private void MakeTrade(ATS.Base_Classes.TradingStructure ts)
        {
            TradeStructure structure = new TradeStructure()
            {
                structure = ts
            };
            int total_count = ts.Purchase.Units;
            if (ts.Purchase.ShortSale)
            {
                foreach (ATS.Base_Classes.TradePoint s in ts.SalePoints.Values)
                {
                    string buy = "EnterShort#" + count++;
                    total_count -= s.Units;
                    EnterShortLimit(0, true, s.Units, ts.Purchase.MaxPrice, buy);
                    SetProfitTarget(buy, CalculationMode.Price, s.Price);
                    SetStopLoss(buy, CalculationMode.Price, ts.Purchase.StopLoss, false);

                    structure.sale_names.Add(buy);
                }
                if (total_count > 0)
                {
                    string buy = "EnterShort#" + count++;
                    EnterShortLimit(0, true, total_count, ts.Purchase.MaxPrice, buy);
                    SetStopLoss(buy, CalculationMode.Price, ts.Purchase.StopLoss, false);

                    structure.sale_names.Add(buy);
                }
            }
            else
            {
                foreach (ATS.Base_Classes.TradePoint s in ts.SalePoints.Values)
                {
                    string buy = "EnterLong#" + count++;
                    total_count -= s.Units;
                    EnterLongLimit(0, true, s.Units, ts.Purchase.MaxPrice, buy);
                    SetProfitTarget(buy, CalculationMode.Price, s.Price);
                    SetStopLoss(buy, CalculationMode.Price, ts.Purchase.StopLoss, false);

                    structure.sale_names.Add(buy);
                }
                if (total_count > 0)
                {
                    string buy = "EnterShort#" + count++;
                    EnterLongLimit(0, true, total_count, ts.Purchase.MaxPrice, buy);
                    SetStopLoss(buy, CalculationMode.Price, ts.Purchase.StopLoss, false);

                    structure.sale_names.Add(buy);
                }
            }
            trades.Add(structure);
        }

        protected override void OnOrderUpdate(IOrder order)
        {
            if (order != null)
            {
                TradeStructure tag_for_removal = null;
                switch (order.OrderType)
                {
                    case OrderType.Limit:
                        if (order.OrderState == OrderState.Filled)
                            foreach (TradeStructure struc in trades)
                            {
                                ATS.Base_Classes.TradePoint lowest_sale = null;
                                string low_name = null;
                                foreach (string name in struc.sale_names)
                                {
                                    if (name == order.FromEntrySignal && struc.structure.Purchase.ShortSale ? order.OrderAction == OrderAction.Buy : order.OrderAction == OrderAction.Sell)
                                    {
                                        foreach (ATS.Base_Classes.TradePoint s in struc.structure.SalePoints.Values)
                                        {
                                            if (lowest_sale == null || s.StopLoss < lowest_sale.StopLoss)
                                                lowest_sale = s;
                                        }
                                        low_name = name;
                                    }
                                }
                                if (low_name != null)
                                    struc.sale_names.Remove(low_name);
                                else
                                {
                                    //ERROR: Find a way to report this.
                                }
                                if (lowest_sale != null)
                                {
                                    if (struc.sale_names.Count < 1)
                                    {
                                        tag_for_removal = struc;
                                        if (struc.sale_names.Count == 1)
                                        {
                                            running_trades.Add(new RunningTrade()
                                            {
                                                shortbuy = struc.structure.Purchase.ShortSale,
                                                name = struc.sale_names[0]
                                            });
                                        }
                                    }
                                    foreach (string name in struc.sale_names)
                                    {
                                        SetStopLoss(name, CalculationMode.Price, lowest_sale.StopLoss, false);
                                    }
                                }
                                else
                                {
                                    //ERROR: Find a way to report this.
                                }
                            }
                        break;
                    case OrderType.StopLimit:
                        foreach (TradeStructure struc in trades)
                        {
                            foreach (string name in struc.sale_names)
                            {
                                if (name == order.FromEntrySignal)
                                    tag_for_removal = struc;
                            }
                        }
                        break;
                }
                if (tag_for_removal != null)
                    trades.Remove(tag_for_removal);
            }
        }

        /// <summary>
        /// Called on each bar update event (incoming tick)
        /// </summary>
        protected override void OnBarUpdate()
        {
            cls.TickSize = TickSize;
            ATS.Base_Classes.BarValue<double> LCurrentBar = new ATS.Base_Classes.BarValue<double>()
            {
                High = High[0],
                Low = Low[0],
                Open = Open[0],
                Close = Close[0]
            };

            swng.BarUpdate(LCurrentBar);
            cls.BarUpdate(LCurrentBar);

            ATS.Base_Classes.SwingEntry high = trck.LastHigh(Instrument.FullName, 1),
                        low = trck.LastLow(Instrument.FullName, 1);
            foreach (RunningTrade trade in running_trades)
            {
                if (trade.shortbuy && high != null)
                {
                    SetStopLoss(trade.name, CalculationMode.Price, high.BarData.High, false);
                }
                else if (!trade.shortbuy && low != null)
                {
                    SetStopLoss(trade.name, CalculationMode.Price, low.BarData.Low, false);
                }
            }
        }

        #region Properties
        [Description("")]
        [GridCategory("Parameters")]
        public int Strength
        {
            get { return strength; }
            set { strength = Math.Max(1, value); }
        }

        [Description("")]
        [GridCategory("Parameters")]
        public int LowFibBound
        {
            get { return lowFibBound; }
            set { lowFibBound = Math.Max(1, value); }
        }

        [Description("")]
        [GridCategory("Parameters")]
        public int UpperFibBound
        {
            get { return upperFibBound; }
            set { upperFibBound = Math.Max(1, value); }
        }
        #endregion
    }
}
