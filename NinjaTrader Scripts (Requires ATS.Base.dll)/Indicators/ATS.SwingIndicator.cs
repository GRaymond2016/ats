/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using ATSI = Automated_Trading_Software.Indicators;
using ATSU = Automated_Trading_Software.Utilities;
using ATSB = Automated_Trading_Software.Base_Classes;
#endregion

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    /// <summary>
    /// Enter the description of your new custom indicator here
    /// </summary>
    [Description("This indicator will measure high / low swings.")]
    public class ATSSwingIndicator : Indicator
    {
        private ATSI.SwingIndicator m_swing;
        public ATSU.SwingTracker Swings { get; set; }

        [Description("Specify the count of bars before a swing is confirmed. (Default: 5)")]
        [Browsable(true)]
        [XmlIgnore()]
        public int Strength
        {
            get { return m_strength; }
            set { m_strength = value; }
        }
        private int m_strength = 5;

        /// <summary>
        /// This method is used to configure the indicator and is called once before any bar data is loaded.
        /// </summary>
        protected override void Initialize()
        {
            Add(new Plot(Color.Black, PlotStyle.Line, "MainPlot"));
            Plots[0].Pen.Width = 1;
            Plots[0].Pen.DashStyle = DashStyle.Solid;
            Overlay = true;
            CalculateOnBarClose = true;
            PriceTypeSupported = false;

            Swings = new ATSU.SwingTracker();
            m_swing = new ATSI.SwingIndicator(Instrument.FullName);
            m_swing.addObserver(_AddSwingHandler);
            m_swing.addObserver(_StateHandler);
            m_swing.Strength = m_strength; //This is exclusive to the current bar.
            m_swing.Begin();
        }

        protected void _AddSwingHandler(String name, ATSB.SwingEntry e)
        {
            Swings.AddSwing(name, e);
        }
        protected void _StateHandler(ATSB.SwingState s)
        {
            Swings.SetCurrentState(s);
        }

        public override void Plot(Graphics graphics, Rectangle bounds, double min, double max)
        {
            if (Bars == null) return;

            List<ATSB.SwingEntry> data = Swings.GiveMeAllPoints(Instrument.FullName);
            data.Sort();

            int x1 = 0, y1 = 0;
            for (int i = 0; i < data.Count - 1 && data[i].BarData != null && data[i + 1].BarData != null; i++)
            {
                int x0 = ChartControl.GetXByBarIdx(Bars, data[i].BarLocation);
                x1 = ChartControl.GetXByBarIdx(Bars, data[i + 1].BarLocation);
                float y0 = ChartControl.GetYByValue(this, (float)(data[i].SwingType == ATSB.SwingState.LowSwing ? data[i].BarData.Low : data[i].BarData.High));
                y1 = ChartControl.GetYByValue(this, (float)(data[i + 1].SwingType == ATSB.SwingState.LowSwing ? data[i + 1].BarData.Low : data[i + 1].BarData.High));

                graphics.DrawLine(Plots[0].Pen, x0, y0, x1, y1);

                if (x0 == x1)
                    graphics.FillPie(new SolidBrush(Color.Brown), x0 - 5, y0 - 5, 10, 10, 0, 360);

                if (i == 0)
                    graphics.FillPie(new SolidBrush(Color.Green), x0 - 5, y0 - 5, 10, 10, 0, 360);
            }

            if (x1 != 0 && y1 != 0)
                graphics.FillPie(new SolidBrush(Color.Red), x1 - 5, y1 - 5, 10, 10, 0, 360);
        }

        /// <summary>
        /// Called on each bar update event (incoming tick)
        /// </summary>
        protected override void OnBarUpdate()
        {
            if (CurrentBar == 0)
            {
                Swings = new ATSU.SwingTracker();
            }
            ATSB.BarValue<double> LCurrentBar = new ATSB.BarValue<double>() { High = High[0], Low = Low[0] };
            m_swing.BarUpdate(LCurrentBar);
        }

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }
        #endregion
    }
}

#region NinjaScript generated code. Neither change nor remove.
// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    public partial class Indicator : IndicatorBase
    {
        private ATSSwingIndicator[] cacheATSSwingIndicator = null;

        private static ATSSwingIndicator checkATSSwingIndicator = new ATSSwingIndicator();

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public ATSSwingIndicator ATSSwingIndicator()
        {
            return ATSSwingIndicator(Input);
        }

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public ATSSwingIndicator ATSSwingIndicator(Data.IDataSeries input)
        {
            if (cacheATSSwingIndicator != null)
                for (int idx = 0; idx < cacheATSSwingIndicator.Length; idx++)
                    if (cacheATSSwingIndicator[idx].EqualsInput(input))
                        return cacheATSSwingIndicator[idx];

            lock (checkATSSwingIndicator)
            {
                if (cacheATSSwingIndicator != null)
                    for (int idx = 0; idx < cacheATSSwingIndicator.Length; idx++)
                        if (cacheATSSwingIndicator[idx].EqualsInput(input))
                            return cacheATSSwingIndicator[idx];

                ATSSwingIndicator indicator = new ATSSwingIndicator();
                indicator.BarsRequired = BarsRequired;
                indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
                indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
                indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
                indicator.Input = input;
                Indicators.Add(indicator);
                indicator.SetUp();

                ATSSwingIndicator[] tmp = new ATSSwingIndicator[cacheATSSwingIndicator == null ? 1 : cacheATSSwingIndicator.Length + 1];
                if (cacheATSSwingIndicator != null)
                    cacheATSSwingIndicator.CopyTo(tmp, 0);
                tmp[tmp.Length - 1] = indicator;
                cacheATSSwingIndicator = tmp;
                return indicator;
            }
        }
    }
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer
{
    public partial class Column : ColumnBase
    {
        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.ATSSwingIndicator ATSSwingIndicator()
        {
            return _indicator.ATSSwingIndicator(Input);
        }

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public Indicator.ATSSwingIndicator ATSSwingIndicator(Data.IDataSeries input)
        {
            return _indicator.ATSSwingIndicator(input);
        }
    }
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    public partial class Strategy : StrategyBase
    {
        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.ATSSwingIndicator ATSSwingIndicator()
        {
            return _indicator.ATSSwingIndicator(Input);
        }

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public Indicator.ATSSwingIndicator ATSSwingIndicator(Data.IDataSeries input)
        {
            if (InInitialize && input == null)
                throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

            return _indicator.ATSSwingIndicator(input);
        }
    }
}
#endregion
