/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using ATSI = Automated_Trading_Software.Indicators;
using ATSU = Automated_Trading_Software.Utilities;
using ATSB = Automated_Trading_Software.Base_Classes;
#endregion

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    /// <summary>
    /// Enter the description of your new custom indicator here
    /// </summary>
    [Description("This indicator will measure trends.")]
    public class ATSTrendIndicator : Indicator
    {
        private ATSI.SwingIndicator m_swing;
        private ATSI.TrendIndicator m_trend;

        private Dictionary<int, ATSB.TrendDirection> m_trends = new Dictionary<int, ATSB.TrendDirection>();
        public ATSU.SwingTracker m_swings = new ATSU.SwingTracker();

        [Description("Specify the count of bars before a swing is confirmed. (Default: 5)")]
        [Browsable(true)]
        [XmlIgnore()]
        public int Strength
        {
            get { return m_strength; }
            set { m_strength = value; }
        }
        private int m_strength = 5;
        private int last_start = 0;

        /// <summary>
        /// This method is used to configure the indicator and is called once before any bar data is loaded.
        /// </summary>
        protected override void Initialize()
        {
            Add(new Plot(Color.MediumBlue, PlotStyle.Bar, "Up"));
            Add(new Plot(Color.Orange, PlotStyle.Bar, "Down"));

            Plots[0].Pen.Width = 1;
            Plots[1].Pen.Width = 1;

            Overlay = false;
            CalculateOnBarClose = true;
            PriceTypeSupported = false;

            m_swing = new ATSI.SwingIndicator(Instrument.FullName);
            m_swing.addObserver(_AddSwingHandler);
            m_swing.Strength = m_strength;
            m_swing.Begin();

            m_trend = new ATSI.TrendIndicator(ref m_swings);
        }

        protected void _AddSwingHandler(String name, ATSB.SwingEntry e)
        {
            m_swings.AddSwing(name, e);
        }

        public override void Plot(Graphics graphics, Rectangle bounds, double min, double max)
        {
            if (Bars == null) return;

            foreach (KeyValuePair<int, ATSB.TrendDirection> pair in m_trends)
            {
                float x0 = ChartControl.GetXByBarIdx(Bars, pair.Key),
                      y0 = ChartControl.GetYByValue(this, 1),
                      y1 = ChartControl.GetYByValue(this, 0);
                graphics.DrawLine(Plots[pair.Value == ATSB.TrendDirection.Up ? 0 : 1].Pen, x0, y0, x0, y1);
            }
        }

        /// <summary>
        /// Called on each bar update event (incoming tick)
        /// </summary>
        protected override void OnBarUpdate()
        {
            ATSB.BarValue<double> LCurrentBar = new ATSB.BarValue<double>() { High = High[0], Low = Low[0], Open = Open[0], Close = Close[0] };
            m_swing.BarUpdate(LCurrentBar);
            m_trend.BarUpdate(LCurrentBar);

            if (m_trend.TrendStart != last_start)
            {
                last_start = m_trend.TrendStart;
                for (int i = last_start; i < CurrentBar; ++i)
                {
                    if (!m_trends.ContainsKey(i))
                        m_trends.Add(i, m_trend.CurrentTrend);
                    else
                        m_trends[i] = m_trend.CurrentTrend;
                }
            }

            if (m_trend.CurrentTrend != ATSB.TrendDirection.NoTrend)
            {
                if (!m_trends.ContainsKey(CurrentBar))
                    m_trends.Add(CurrentBar, m_trend.CurrentTrend);
                else
                    m_trends[CurrentBar] = m_trend.CurrentTrend;
            }
        }

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }
        #endregion
    }
}

#region NinjaScript generated code. Neither change nor remove.
// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    public partial class Indicator : IndicatorBase
    {
        private ATSTrendIndicator[] cacheATSTrendIndicator = null;

        private static ATSTrendIndicator checkATSTrendIndicator = new ATSTrendIndicator();

        /// <summary>
        /// This indicator will measure trends.
        /// </summary>
        /// <returns></returns>
        public ATSTrendIndicator ATSTrendIndicator()
        {
            return ATSTrendIndicator(Input);
        }

        /// <summary>
        /// This indicator will measure trends.
        /// </summary>
        /// <returns></returns>
        public ATSTrendIndicator ATSTrendIndicator(Data.IDataSeries input)
        {
            if (cacheATSTrendIndicator != null)
                for (int idx = 0; idx < cacheATSTrendIndicator.Length; idx++)
                    if (cacheATSTrendIndicator[idx].EqualsInput(input))
                        return cacheATSTrendIndicator[idx];

            lock (checkATSTrendIndicator)
            {
                if (cacheATSTrendIndicator != null)
                    for (int idx = 0; idx < cacheATSTrendIndicator.Length; idx++)
                        if (cacheATSTrendIndicator[idx].EqualsInput(input))
                            return cacheATSTrendIndicator[idx];

                ATSTrendIndicator indicator = new ATSTrendIndicator();
                indicator.BarsRequired = BarsRequired;
                indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
                indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
                indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
                indicator.Input = input;
                Indicators.Add(indicator);
                indicator.SetUp();

                ATSTrendIndicator[] tmp = new ATSTrendIndicator[cacheATSTrendIndicator == null ? 1 : cacheATSTrendIndicator.Length + 1];
                if (cacheATSTrendIndicator != null)
                    cacheATSTrendIndicator.CopyTo(tmp, 0);
                tmp[tmp.Length - 1] = indicator;
                cacheATSTrendIndicator = tmp;
                return indicator;
            }
        }
    }
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer
{
    public partial class Column : ColumnBase
    {
        /// <summary>
        /// This indicator will measure trends.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.ATSTrendIndicator ATSTrendIndicator()
        {
            return _indicator.ATSTrendIndicator(Input);
        }

        /// <summary>
        /// This indicator will measure trends.
        /// </summary>
        /// <returns></returns>
        public Indicator.ATSTrendIndicator ATSTrendIndicator(Data.IDataSeries input)
        {
            return _indicator.ATSTrendIndicator(input);
        }
    }
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    public partial class Strategy : StrategyBase
    {
        /// <summary>
        /// This indicator will measure trends.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.ATSTrendIndicator ATSTrendIndicator()
        {
            return _indicator.ATSTrendIndicator(Input);
        }

        /// <summary>
        /// This indicator will measure trends.
        /// </summary>
        /// <returns></returns>
        public Indicator.ATSTrendIndicator ATSTrendIndicator(Data.IDataSeries input)
        {
            if (InInitialize && input == null)
                throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

            return _indicator.ATSTrendIndicator(input);
        }
    }
}
#endregion
