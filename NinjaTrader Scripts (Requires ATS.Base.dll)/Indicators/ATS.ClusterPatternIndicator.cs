/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Collections.Generic;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using ATSI = Automated_Trading_Software.Indicators;
using ATSU = Automated_Trading_Software.Utilities;
using ATSB = Automated_Trading_Software.Base_Classes;
using ATSS = Automated_Trading_Software.Strategies;
#endregion

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    /// <summary>
    /// Enter the description of your new custom indicator here
    /// </summary>
    [Description("This indicator will show potential cluster patterns.")]
    public class ATSClusterPatternIndicator : Indicator
    {
        private ATSI.SwingIndicator m_swing;
        private ATSI.ClusteringIndicator m_cluster = null;
        private class TradeDrawStructure
        {
            public int Bar;
            public double Price;
            public double StopLoss;
        };
        private List<TradeDrawStructure> m_trades = new List<TradeDrawStructure>();
        private Dictionary<int, ATSB.ClusteringStructure> m_clusters = new Dictionary<int, ATSB.ClusteringStructure>();
        private ATSU.SwingTracker m_swings = new ATSU.SwingTracker();

        [Description("Specify the count of bars before a swing is confirmed. (Default: 5)")]
        [Browsable(true)]
        [XmlIgnore()]
        public int Strength
        {
            get { return m_strength; }
            set { m_strength = value; }
        }
        private int m_strength = 5;

        [Description("Specify the risk per trade in percent. (Default: 100)")]
        [Browsable(true)]
        [XmlIgnore()]
        public int Risk
        {
            get { return m_risk; }
            set { m_risk = value; }
        }
        private int m_risk = 100;

        [Description("Specify the value fibonacci retracement to be used as a higher bound in clustering. (Default: 121)")]
        [Browsable(true)]
        [XmlIgnore()]
        public float FibUpperBound
        {
            get { return m_fibUpper; }
            set { m_fibUpper = value; }
        }
        private float m_fibUpper = 121f;

        [Description("Specify the value fibonacci retracement to be used as a lower bound in clustering. (Default: 162)")]
        [Browsable(true)]
        [XmlIgnore()]
        public float FibLowerBound
        {
            get { return m_fibLower; }
            set { m_fibLower = value; }
        }
        private float m_fibLower = 162f;

        [Description("Specify displaying of trades. (buy, sell & stoploss)")]
        [Browsable(true)]
        [XmlIgnore]
        public bool ShowTrades
        {
            get { return s_trades; }
            set { s_trades = value; }
        }
        private bool s_trades = false;

        [Description("Percentage of AB that the BC line must retrace for the structure to be valid.")]
        [Browsable(true)]
        [XmlIgnore]
        public double BCRatio
        {
            get { return m_bcratio; }
            set { m_bcratio = value; }
        }
        private double m_bcratio = 0.5;

        [Description("Percentage of BC that the CD line must retrace for the structure to be valid.")]
        [Browsable(true)]
        [XmlIgnore]
        public double CDRatio
        {
            get { return m_cdratio; }
            set { m_cdratio = value; }
        }
        private double m_cdratio = 0.5;

        [Description("Percentage of XA that the AB line must retrace for the structure to be valid.")]
        [Browsable(true)]
        [XmlIgnore]
        public double ABRatio
        {
            get { return m_abratio; }
            set { m_abratio = value; }
        }
        private double m_abratio = 0.1;

        [Description("Require that equality be in the cluster zone to recognize a structure.")]
        [Browsable(true)]
        [XmlIgnore]
        public bool RequireEquality
        {
            get { return m_requality; }
            set { m_requality = value; }
        }
        private bool m_requality = false;

        [Description("Draw the cluster zone.")]
        [Browsable(true)]
        [XmlIgnore]
        public bool DrawClusterZone
        {
            get { return m_drawcluster; }
            set { m_drawcluster = value; }
        }
        private bool m_drawcluster = true;

        [Description("Require the cluster zone to be partially within 50 - 76.5 retracement of AX.")]
        [Browsable(true)]
        [XmlIgnore]
        public bool UseClusterBounds
        {
            get { return m_clusterBounds; }
            set { m_clusterBounds = value; }
        }
        private bool m_clusterBounds = true;

        [Description("Specify the value fibonacci retracement of XA to be used as a higher bound for B to enter. (Default: 50)")]
        [Browsable(true)]
        [XmlIgnore()]
        public float FibABUpperBound
        {
            get { return m_ABfibUpper; }
            set { m_ABfibUpper = value; }
        }
        private float m_ABfibUpper = 50f;

        [Description("Specify the value fibonacci retracement of XA to be used as a lower bound for B to enter. (Default: 31.5)")]
        [Browsable(true)]
        [XmlIgnore()]
        public float FibABLowerBound
        {
            get { return m_ABfibLower; }
            set { m_ABfibLower = value; }
        }
        private float m_ABfibLower = 31.5f;

        [Description("The number of bars that must close within the cluster before a trade happens.")]
        [Browsable(true)]
        [XmlIgnore()]
        public int CloseCount
        {
            get { return m_closeCount; }
            set { m_closeCount = value; }
        }
        private int m_closeCount = 3;

        /// <summary>
        /// This method is used to configure the indicator and is called once before any bar data is loaded.
        /// </summary>
        protected override void Initialize()
        {
            Add(new Plot(Color.MediumBlue, PlotStyle.Line, "StructurePlot"));
            Add(new Plot(Color.Pink, PlotStyle.Line, "RulePlot"));

            Plots[0].Pen.Width = 3;

            Overlay = true;
            CalculateOnBarClose = true;
            PriceTypeSupported = false;

            m_swing = new ATSI.SwingIndicator(Instrument.FullName);
            m_swing.addObserver(_AddSwingHandler);
            m_swing.addObserver(_StateHandler);
            m_swing.Strength = m_strength;
            m_swing.Begin();

            m_cluster = new ATSI.ClusteringIndicator(ref m_swings, Instrument.FullName);

            ATSI.ClusteringIndicator.ClusterHigh = m_fibUpper;
            ATSI.ClusteringIndicator.ClusterLow = m_fibLower;
            m_cluster.addObserver(ClusterHandler);
            m_cluster.addObserver(_TradeHandler);

            m_cluster.XAtoABRatio = ABRatio;
            m_cluster.ABtoBCRatio = BCRatio;
            m_cluster.BCtoCDRatio = CDRatio;
            m_cluster.CloseCount = CloseCount;
            m_cluster.ABFibHighRetracement = FibABUpperBound;
            m_cluster.ABFibLowRetracement = FibABLowerBound;
            m_cluster.RequireClusterBounds = UseClusterBounds;
            m_cluster.RequireEquality = RequireEquality;

            ATSS.PurchaseTracker.Advisor.FundRetrieval = GetFunds;
            ATSS.PurchaseTracker.Advisor.Risk = Risk;
        }

        private double GetFunds()
        {
            return 100000;
        }
        protected void _AddSwingHandler(String name, ATSB.SwingEntry e)
        {
            m_swings.AddSwing(name, e);
        }
        protected void _StateHandler(ATSB.SwingState s)
        {
            m_swings.SetCurrentState(s);
        }
        protected void _TradeHandler(ATSB.TradingStructure s)
        {
            m_trades.Add(new TradeDrawStructure() { Bar = CurrentBar, Price = s.Purchase.Price, StopLoss = s.Purchase.StopLoss });
            foreach (KeyValuePair<ATSB.BuyPosition, ATSB.TradePoint> sell in s.SalePoints)
                m_trades.Add(new TradeDrawStructure() { Bar = CurrentBar, Price = sell.Value.Price, StopLoss = sell.Value.StopLoss });
        }

        protected void ClusterHandler(ATSB.ClusteringStructure e)
        {
            m_clusters[e.Origin.BarLocation] = e;
        }

        public override void Plot(Graphics graphics, Rectangle bounds, double min, double max)
        {
            if (Bars == null) return;

            foreach (ATSB.ClusteringStructure e in m_clusters.Values)
            {
                DrawLine(graphics, e.Origin, e.X, Plots[0].Pen);
                DrawDot(graphics, e.Origin, Plots[0].Pen.Color);
                DrawText(graphics, e.Origin, "O");
                DrawDot(graphics, e.X, Plots[0].Pen.Color);
                DrawLine(graphics, e.X, e.A, Plots[0].Pen);
                DrawText(graphics, e.X, "X");
                DrawDot(graphics, e.A, Plots[0].Pen.Color);
                DrawLine(graphics, e.A, e.B, Plots[0].Pen);
                DrawText(graphics, e.A, "A");
                DrawDot(graphics, e.B, Plots[0].Pen.Color);
                DrawLine(graphics, e.B, e.C, Plots[0].Pen);
                DrawText(graphics, e.B, "B");
                DrawDot(graphics, e.C, Plots[0].Pen.Color);
                DrawText(graphics, e.C, "C");
                if (e.D != null)
                {
                    ATSB.SwingEntry finalPoint = new ATSB.SwingEntry()
                    {
                        //This is not actually a low swing, but we want to draw it as a confirmed low.
                        SwingType = e.C.BarData.Low < e.D.BarData.Low ? ATSB.SwingState.HighSwing : ATSB.SwingState.LowSwing,
                        BarData = e.D.BarData,
                        BarLocation = e.D.BarLocation
                    };
                    DrawLine(graphics, e.C, finalPoint, Plots[0].Pen);
                    DrawDot(graphics, finalPoint, Plots[0].Pen.Color);
                    DrawText(graphics, finalPoint, "D");
                }

                if (m_requality)
                    DrawLine(graphics, (int)e.Cluster.Left, e.Equality, (int)e.Cluster.Right, e.Equality, Plots[1].Pen);

                DrawLine(graphics, (int)e.Cluster.Left, e.ClusterMax, (int)e.Cluster.Right, e.ClusterMax, Plots[1].Pen);
                DrawLine(graphics, (int)e.Cluster.Left, e.ClusterMin, (int)e.Cluster.Right, e.ClusterMin, Plots[1].Pen);
                DrawLine(graphics, (int)e.B.BarLocation - 5, e.AXLowRetracement, (int)e.B.BarLocation + 5, e.AXLowRetracement, Plots[1].Pen);
                DrawLine(graphics, (int)e.B.BarLocation - 5, e.AXHighRetracement, (int)e.B.BarLocation + 5, e.AXHighRetracement, Plots[1].Pen);

                if (m_drawcluster)
                    DrawBox(graphics, (float)e.Cluster.Left, (float)e.Cluster.Top, (float)e.Cluster.Right, (float)e.Cluster.Bottom, Plots[0].Pen);
            }

            graphics.DrawString("Cluster Zones: " + m_clusters.Count, new Font("Arial", 12, FontStyle.Regular), Plots[0].Pen.Brush, 0, 20);

            if (s_trades)
            {
                foreach (var marker in m_trades)
                {
                    ATSB.SwingEntry pointofsale = new ATSB.SwingEntry()
                    {
                        BarLocation = marker.Bar,
                        BarData = new ATSB.BarValue<double>()
                        {
                            High = marker.Price,
                            Low = marker.Price,
                            Open = marker.Price,
                            Close = marker.Price
                        },
                        SwingType = ATSB.SwingState.HighSwing
                    };
                    ATSB.SwingEntry stoploss = new ATSB.SwingEntry()
                    {
                        BarLocation = marker.Bar,
                        BarData = new ATSB.BarValue<double>()
                        {
                            High = marker.StopLoss,
                            Low = marker.StopLoss,
                            Open = marker.StopLoss,
                            Close = marker.StopLoss
                        },
                        SwingType = ATSB.SwingState.HighSwing
                    };
                    DrawDot(graphics, pointofsale, Color.Purple);
                    DrawDot(graphics, stoploss, Color.DarkOliveGreen);
                }
            }
        }
        private void DrawText(Graphics graphics, ATSB.SwingEntry structure, string text)
        {
            Font f = new Font(FontFamily.GenericSansSerif, 10);
            Brush b = new SolidBrush(Color.Black);

            SizeF size = graphics.MeasureString(text, f);

            int x = ChartControl.GetXByBarIdx(Bars, structure.BarLocation) - (int)(size.Width / 2);
            float y = (structure.SwingType == ATSB.SwingState.HighSwing ? -size.Height - 5 : 5) +
                            ChartControl.GetYByValue(this, GetSwingValue(structure));

            graphics.DrawString(text, f, b, x, y);
        }
        private void DrawBox(Graphics graphics, float x, float y, float x1, float y1, Pen p)
        {
            int ax = ChartControl.GetXByBarIdx(Bars, (int)x),
                ax1 = ChartControl.GetXByBarIdx(Bars, (int)x1);
            float ay = ChartControl.GetYByValue(this, y),
                  ay1 = ChartControl.GetYByValue(this, y1);

            graphics.DrawRectangle(p, ax, ay, ax1 - ax, ay1 - ay);
        }
        private void DrawDot(Graphics graphics, ATSB.SwingEntry a, Color col)
        {
            int x0 = ChartControl.GetXByBarIdx(Bars, a.BarLocation);
            float y0 = ChartControl.GetYByValue(this, GetSwingValue(a));

            graphics.FillPie(new SolidBrush(col), x0 - 5, y0 - 5, 10, 10, 0, 360);
        }
        private void DrawLine(Graphics graphics, ATSB.SwingEntry a, ATSB.SwingEntry b, Pen pen)
        {
            double y0 = GetSwingValue(a),
                  y1 = GetSwingValue(b);
            DrawLine(graphics, a.BarLocation, y0, b.BarLocation, y1, pen);
        }
        private double GetSwingValue(ATSB.SwingEntry a)
        {
            if (a.SwingType == ATSB.SwingState.HighSwing)
                return a.BarData.High;
            else if (a.SwingType == ATSB.SwingState.LowSwing)
                return a.BarData.Low;

            throw new Exception("Invalid Swing Type");
        }
        private void DrawLine(Graphics graphics, int bar0, double value0, int bar1, double value1, Pen pen)
        {
            int x0 = ChartControl.GetXByBarIdx(Bars, bar0),
                x1 = ChartControl.GetXByBarIdx(Bars, bar1);
            float y0 = ChartControl.GetYByValue(this, value0),
                    y1 = ChartControl.GetYByValue(this, value1);

            graphics.DrawLine(pen, x0, y0, x1, y1);
        }

        /// <summary>
        /// Called on each bar update event (incoming tick)
        /// </summary>
        protected override void OnBarUpdate()
        {
            m_cluster.TickSize = TickSize;
            ATSB.BarValue<double> LCurrentBar = new ATSB.BarValue<double>()
            {
                High = High[0],
                Low = Low[0],
                Open = Open[0],
                Close = Close[0]
            };
            m_swing.BarUpdate(LCurrentBar);

            switch (BarsPeriod.BasePeriodType)
            {
                case PeriodType.Second: m_cluster.BarResolution = BarsPeriod.Value / 60; break;
                case PeriodType.Minute: m_cluster.BarResolution = BarsPeriod.Value; break;
                default: throw new ArgumentException("Could not resolve the bar period type.");
            }
            m_cluster.BarUpdate(LCurrentBar);
        }

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }
        #endregion
    }
}

#region NinjaScript generated code. Neither change nor remove.
// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    public partial class Indicator : IndicatorBase
    {
        private ATSClusterPatternIndicator[] cacheATSClusterPatternIndicator = null;

        private static ATSClusterPatternIndicator checkATSClusterPatternIndicator = new ATSClusterPatternIndicator();

        /// <summary>
        /// This indicator will show potential cluster patterns.
        /// </summary>
        /// <returns></returns>
        public ATSClusterPatternIndicator ATSClusterPatternIndicator()
        {
            return ATSClusterPatternIndicator(Input);
        }

        /// <summary>
        /// This indicator will show potential cluster patterns.
        /// </summary>
        /// <returns></returns>
        public ATSClusterPatternIndicator ATSClusterPatternIndicator(Data.IDataSeries input)
        {
            if (cacheATSClusterPatternIndicator != null)
                for (int idx = 0; idx < cacheATSClusterPatternIndicator.Length; idx++)
                    if (cacheATSClusterPatternIndicator[idx].EqualsInput(input))
                        return cacheATSClusterPatternIndicator[idx];

            lock (checkATSClusterPatternIndicator)
            {
                if (cacheATSClusterPatternIndicator != null)
                    for (int idx = 0; idx < cacheATSClusterPatternIndicator.Length; idx++)
                        if (cacheATSClusterPatternIndicator[idx].EqualsInput(input))
                            return cacheATSClusterPatternIndicator[idx];

                ATSClusterPatternIndicator indicator = new ATSClusterPatternIndicator();
                indicator.BarsRequired = BarsRequired;
                indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
                indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
                indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
                indicator.Input = input;
                Indicators.Add(indicator);
                indicator.SetUp();

                ATSClusterPatternIndicator[] tmp = new ATSClusterPatternIndicator[cacheATSClusterPatternIndicator == null ? 1 : cacheATSClusterPatternIndicator.Length + 1];
                if (cacheATSClusterPatternIndicator != null)
                    cacheATSClusterPatternIndicator.CopyTo(tmp, 0);
                tmp[tmp.Length - 1] = indicator;
                cacheATSClusterPatternIndicator = tmp;
                return indicator;
            }
        }
    }
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer
{
    public partial class Column : ColumnBase
    {
        /// <summary>
        /// This indicator will show potential cluster patterns.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.ATSClusterPatternIndicator ATSClusterPatternIndicator()
        {
            return _indicator.ATSClusterPatternIndicator(Input);
        }

        /// <summary>
        /// This indicator will show potential cluster patterns.
        /// </summary>
        /// <returns></returns>
        public Indicator.ATSClusterPatternIndicator ATSClusterPatternIndicator(Data.IDataSeries input)
        {
            return _indicator.ATSClusterPatternIndicator(input);
        }
    }
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    public partial class Strategy : StrategyBase
    {
        /// <summary>
        /// This indicator will show potential cluster patterns.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.ATSClusterPatternIndicator ATSClusterPatternIndicator()
        {
            return _indicator.ATSClusterPatternIndicator(Input);
        }

        /// <summary>
        /// This indicator will show potential cluster patterns.
        /// </summary>
        /// <returns></returns>
        public Indicator.ATSClusterPatternIndicator ATSClusterPatternIndicator(Data.IDataSeries input)
        {
            if (InInitialize && input == null)
                throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

            return _indicator.ATSClusterPatternIndicator(input);
        }
    }
}
#endregion
