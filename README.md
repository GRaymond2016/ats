# README #

You need NinjaTrader and to put the scripts in your NinjaTrader folder. You will also need to load the built library into NinjaTrader for the scripts to run properly.

### What is this repository for? ###

This is an automated trading platform that detects structures in data given by NinjaTrader and trades based on it.