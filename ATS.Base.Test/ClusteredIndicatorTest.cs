﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Trading_Software.Base_Classes;
using System.IO;
using Automated_Trading_Software.Utilities;
using Automated_Trading_Software.Indicators;
using Automated_Trading_Software.Strategies;

namespace ATS.Base.Test
{
    [TestClass]
    public class ClusteredIndicatorTest
    {
        private List<BarValue<double>> vals = new List<BarValue<double>>();
        [TestMethod]
        public void testIndicatorOutputForLongSell()
        {
            using (FileStream fs = new FileStream("..\\..\\..\\testPattern.txt", FileMode.Open, FileAccess.Read))
            using (StreamReader read = new StreamReader(fs))
            {
                while (!read.EndOfStream)
                {
                    String line = read.ReadLine();
                    String[] elements = line.Split(new char[] { ',' });
                    for (int i = 1; i < elements.Length; ++i)
                    {
                        BarValue<double> value = new BarValue<double>();
                        value.Open = double.Parse(elements[i++]);
                        value.Low = double.Parse(elements[i++]);
                        value.High = double.Parse(elements[i++]);
                        value.Close = double.Parse(elements[i++]);
                        vals.Add(value);
                    }
                }
            }

            SwingTracker track = new SwingTracker();
            SwingIndicator swing = new SwingIndicator();
            ClusteringIndicator cluster = new ClusteringIndicator(ref track, "TestFixture1");

            swing.Begin();
            swing.Strength = 5;
            swing.addObserver(track.AddSwing);
            swing.addObserver(track.SetCurrentState);

            cluster.Begin();
            cluster.addObserver(MakeTrade);

            PurchaseTracker.Advisor.FundRetrieval = GetFunds;
            PurchaseTracker.Advisor.Risk = 6;
            ClusteringIndicator.RuleSet = PurchasingRuleSet.GetMediumInvestmentRuleSet();

            cluster.TickSize = 0.25;

            foreach (BarValue<double> values in vals)
            {
                swing.BarUpdate(values);
                cluster.BarUpdate(values);
            }
        }

        private double GetFunds()
        {
            return 100000;
        }

        private void MakeTrade(TradingStructure ts)
        {
            Assert.IsTrue(ts.Purchase.StopLoss < ts.Purchase.Price, "Stop loss was greater than Price");
            Assert.IsTrue(ts.Purchase.MaxPrice > ts.Purchase.Price, "Max Price was greater than Price");
            Assert.IsTrue(ts.Purchase.Units == 3, "Incorrect count of units bought");
            Assert.IsTrue(ts.Purchase.ShortSale == false, "Invalid short sale");
            foreach (var t in ts.SalePoints)
            {
                Assert.IsTrue(t.Value.StopLoss < t.Value.Price, "Stop loss was greater than Price");
                Assert.IsTrue(t.Value.Units == 1, "Incorrect count of units bought");
                Assert.IsTrue(t.Value.ShortSale == false, "Invalid short sale");
            }
        }
    }
}
