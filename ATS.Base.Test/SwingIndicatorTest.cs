﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Trading_Software.Utilities;
using Automated_Trading_Software.Indicators;
using Automated_Trading_Software.Base_Classes;

namespace ATS.Base.Test
{
    [TestClass]
    public class SwingIndicatorTest
    {
        //It might be usefull to have our own simulator method for putting
        //in historical data, even if just for testing purposes.
        BarValue<double>[] testHigh1 = new BarValue<double>[] 
        { 
            new BarValue<double>(){ High = 1.0, Low = 0.8 },
            new BarValue<double>(){ High = 1.1, Low = 0.9 },
            new BarValue<double>(){ High = 1.2, Low = 1.0 },
            new BarValue<double>(){ High = 1.3, Low = 1.1 },
            new BarValue<double>(){ High = 1.4, Low = 1.2 } //Confirm Point - High
        };
        BarValue<double> confirmPointTestHigh1 = new BarValue<double>() { High = 1.4, Low = 1.2 };
        BarValue<double>[] testLow1 = new BarValue<double>[] 
        { 
            new BarValue<double>(){ High = 1.4, Low = 1.2 }, //Swing High
            new BarValue<double>(){ High = 1.3, Low = 1.1 },
            new BarValue<double>(){ High = 1.2, Low = 1.0 },
            new BarValue<double>(){ High = 1.1, Low = 0.9 }, //Confirm Point - Low
            new BarValue<double>(){ High = 1.0, Low = 0.8 },
            new BarValue<double>(){ High = 0.9, Low = 0.7 },
            new BarValue<double>(){ High = 0.8, Low = 0.6 } //Swing Low
        };
        BarValue<double>[] testHigh2 = new BarValue<double>[] 
        { 
            new BarValue<double>(){ High = 0.9, Low = 0.7 },
            new BarValue<double>(){ High = 1.0, Low = 0.8 },
            new BarValue<double>(){ High = 1.1, Low = 0.9 }, //Confirm Point - High
            new BarValue<double>(){ High = 1.2, Low = 1.0 },
            new BarValue<double>(){ High = 1.3, Low = 1.1 },
            new BarValue<double>(){ High = 1.4, Low = 1.2 },
            new BarValue<double>(){ High = 1.5, Low = 1.3 },
            new BarValue<double>(){ High = 1.6, Low = 1.4 } //Swing High
        };
        BarValue<double>[] testLow2 = new BarValue<double>[] 
        { 
            new BarValue<double>(){ High = 1.5, Low = 1.3 },
            new BarValue<double>(){ High = 1.4, Low = 1.2 },
            new BarValue<double>(){ High = 1.3, Low = 1.1 }, //Confirm Point - Low
            new BarValue<double>(){ High = 1.2, Low = 1.0 }
        };

        void PopulateEntire(SwingIndicator sw)
        {
            foreach (BarValue<double> ls in testHigh1)
                sw.BarUpdate(ls);
            foreach (BarValue<double> ls in testLow1)
                sw.BarUpdate(ls);
            foreach (BarValue<double> ls in testHigh2)
                sw.BarUpdate(ls);
            foreach (BarValue<double> ls in testLow2)
                sw.BarUpdate(ls);
        }

        [TestMethod]
        public void TestSwingPoints()
        {
            SwingIndicator sw = new SwingIndicator();
            SwingTracker st = new SwingTracker();
            sw.addObserver(delegate(SwingEntry e) { st.AddSwing(e); });
            sw.Begin();
            PopulateEntire(sw);
            List<SwingEntry> values = st.GiveMeAllPoints();

            Assert.AreEqual(3, values.Count);

            Assert.AreEqual(values[0].SwingType, SwingState.HighSwing);
            Assert.AreEqual(values[0].BarLocation, 6);
            Assert.AreEqual(values[0].BarData.High, testLow1[0].High);
            Assert.AreEqual(values[0].BarData.Low, testLow1[0].Low);

            Assert.AreEqual(values[1].SwingType, SwingState.LowSwing);
            Assert.AreEqual(values[1].BarLocation, 12);
            Assert.AreEqual(values[1].BarData.High, testLow1[6].High);
            Assert.AreEqual(values[1].BarData.Low, testLow1[6].Low);

            Assert.AreEqual(values[2].SwingType, SwingState.HighSwing);
            Assert.AreEqual(values[2].BarLocation, 20);
            Assert.AreEqual(values[2].BarData.High, testHigh2[7].High);
            Assert.AreEqual(values[2].BarData.Low, testHigh2[7].Low);
        }
        [TestMethod]
        public void TestSwingAccessors()
        {
            SwingIndicator sw = new SwingIndicator();
            SwingTracker st = new SwingTracker();
            sw.addObserver(delegate(SwingEntry e) { st.AddSwing(e); });
            sw.Begin();
            PopulateEntire(sw);

            SwingEntry swingLow = st.LastHigh();
            SwingEntry sweetChariot = st.LastLow();
            SwingEntry mommasGonnaCarryMeHome = st.LastHigh(2);

            Assert.AreEqual(mommasGonnaCarryMeHome.SwingType, SwingState.HighSwing);
            Assert.AreEqual(mommasGonnaCarryMeHome.BarLocation, 6);
            Assert.AreEqual(mommasGonnaCarryMeHome.BarData.High, testLow1[0].High);
            Assert.AreEqual(mommasGonnaCarryMeHome.BarData.Low, testLow1[0].Low);

            Assert.AreEqual(sweetChariot.SwingType, SwingState.LowSwing);
            Assert.AreEqual(sweetChariot.BarLocation, 12);
            Assert.AreEqual(sweetChariot.BarData.High, testLow1[6].High);
            Assert.AreEqual(sweetChariot.BarData.Low, testLow1[6].Low);

            Assert.AreEqual(swingLow.SwingType, SwingState.HighSwing);
            Assert.AreEqual(swingLow.BarLocation, 20);
            Assert.AreEqual(swingLow.BarData.High, testHigh2[7].High);
            Assert.AreEqual(swingLow.BarData.Low, testHigh2[7].Low);
        }
        [TestMethod]
        public void ManualTestSwingTrackedPoints()
        {
            SwingIndicator sw = new SwingIndicator();
            sw.Begin();

            foreach (BarValue<double> ls in testHigh1)
                sw.BarUpdate(ls);

            Assert.AreEqual(sw.LastConfirmedEntry.SwingType, SwingState.HighSwing);
            Assert.AreEqual(sw.LastConfirmedEntry.BarLocation, 5);
            Assert.AreEqual(sw.LastConfirmedEntry.BarData.Low, confirmPointTestHigh1.Low);
            Assert.AreEqual(sw.LastConfirmedEntry.BarData.High, confirmPointTestHigh1.High);

            SwingEntry extremeOfLastSwing = new SwingEntry();
            sw.addObserver(delegate(SwingEntry e) { extremeOfLastSwing = e; });
            for (int i = 0; i < testLow1.Length; ++i)
            {
                sw.BarUpdate(testLow1[i]);
                if (i == 3)
                {
                    Assert.AreEqual(sw.LastConfirmedEntry.SwingType, SwingState.LowSwing);
                    Assert.AreEqual(sw.LastConfirmedEntry.BarLocation, 9);
                    Assert.AreEqual(sw.LastConfirmedEntry.BarData.High, testLow1[i].High);
                    Assert.AreEqual(sw.LastConfirmedEntry.BarData.Low, testLow1[i].Low);

                    Assert.AreEqual(extremeOfLastSwing.SwingType, SwingState.HighSwing);
                    Assert.AreEqual(extremeOfLastSwing.BarLocation, 6);
                    Assert.AreEqual(extremeOfLastSwing.BarData.High, testLow1[0].High);
                    Assert.AreEqual(extremeOfLastSwing.BarData.Low, testLow1[0].Low);
                    extremeOfLastSwing = new SwingEntry();
                }
            }
            for (int i = 0; i < testHigh2.Length; ++i)
            {
                sw.BarUpdate(testHigh2[i]);
                if (i == 2)
                {
                    Assert.AreEqual(sw.LastConfirmedEntry.SwingType, SwingState.HighSwing);
                    Assert.AreEqual(sw.LastConfirmedEntry.BarLocation, 15);
                    Assert.AreEqual(sw.LastConfirmedEntry.BarData.High, testHigh2[i].High);
                    Assert.AreEqual(sw.LastConfirmedEntry.BarData.Low, testHigh2[i].Low);

                    Assert.AreEqual(extremeOfLastSwing.SwingType, SwingState.LowSwing);
                    Assert.AreEqual(extremeOfLastSwing.BarLocation, 12);
                    Assert.AreEqual(extremeOfLastSwing.BarData.High, testLow1[6].High);
                    Assert.AreEqual(extremeOfLastSwing.BarData.Low, testLow1[6].Low);
                    extremeOfLastSwing = new SwingEntry();
                }
            }
            for (int i = 0; i < testLow2.Length; ++i)
            {
                sw.BarUpdate(testLow2[i]);
            }

            Assert.AreEqual(sw.LastConfirmedEntry.SwingType, SwingState.LowSwing);
            Assert.AreEqual(sw.LastConfirmedEntry.BarLocation, 23);
            Assert.AreEqual(sw.LastConfirmedEntry.BarData.High, testLow2[2].High);
            Assert.AreEqual(sw.LastConfirmedEntry.BarData.Low, testLow2[2].Low);

            Assert.AreEqual(extremeOfLastSwing.SwingType, SwingState.HighSwing);
            Assert.AreEqual(extremeOfLastSwing.BarLocation, 20);
            Assert.AreEqual(extremeOfLastSwing.BarData.High, testHigh2[7].High);
            Assert.AreEqual(extremeOfLastSwing.BarData.Low, testHigh2[7].Low);
        }
    }
}
