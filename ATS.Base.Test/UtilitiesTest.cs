﻿/*
 * Copyright 2013 Gregory Raymond and Stuart Mills

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 *      
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. */

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Automated_Trading_Software.Utilities;
using Automated_Trading_Software.Base_Classes;

namespace ATS.Base.Test
{
    [TestClass]
    public class UtilitiesTest
    {
        [TestMethod]
        public void TestBarValue()
        {
            BarValue<int> testint = new BarValue<int>();
            Assert.AreEqual(0, testint.Close);
            Assert.AreEqual(0, testint.High);
            Assert.AreEqual(0, testint.Low);
            Assert.AreEqual(0, testint.Open);

            BarValue<double> testdouble = new BarValue<double>();
            Assert.AreEqual(0.0, testdouble.Close);
            Assert.AreEqual(0.0, testdouble.Open);
            Assert.AreEqual(0.0, testdouble.High);
            Assert.AreEqual(0.0, testdouble.Low);

            BarValue<double> testTemplate = new BarValue<double>()
            {
                High = 1.1,
                Low = 2.2,
                Open = 3.3,
                Close = 4.4
            };

            BarValue<double> testSetDouble = new BarValue<double>(testTemplate);
            Assert.AreEqual(testSetDouble.High, testTemplate.High);
            Assert.AreEqual(testSetDouble.Low, testTemplate.Low);
            Assert.AreEqual(testSetDouble.Close, testTemplate.Close);
            Assert.AreEqual(testSetDouble.Open, testTemplate.Open);

            Assert.AreNotSame(testSetDouble.High, testTemplate.High);
            Assert.AreNotSame(testSetDouble.Low, testTemplate.Low);
            Assert.AreNotSame(testSetDouble.Open, testTemplate.Open);
            Assert.AreNotSame(testSetDouble.Close, testTemplate.Close);
            Assert.AreNotSame(testSetDouble, testTemplate);
        }
        [TestMethod]
        public void TestScreenCap()
        {
            ScreenCapture sc = new ScreenCapture();
            sc.CaptureActiveScreen();
        }
        [TestMethod]
        public void TestRollingInt()
        {
            const int max = 5;
            RollingInt roll = new RollingInt(max);
            Assert.AreEqual(0, (int)roll);
            Assert.AreEqual(1, (int)++roll);
            Assert.AreEqual(1, (int)roll);
            //In C# this ++ operation occurs within the bounds of the function call.
            //aka Assert.AreEqual(1, roll++); is invalid;
            roll++;
            Assert.AreEqual(2, (int)roll);

            Assert.AreEqual(1, (int)--roll);
            Assert.AreEqual(1, (int)roll);
            //In C# this ++ operation occurs within the bounds of the function call. 
            //aka Assert.AreEqual(1, roll--); is invalid;
            roll--;
            Assert.AreEqual(0, (int)roll);

            for (int i = 0; i < max * 2; ++i)
                roll++;
            Assert.AreEqual(0, (int)roll);

            for (int i = 0; i < max * 2; ++i)
                roll--;
            Assert.AreEqual(0, (int)roll);

            for (int i = 0; i < max * 2; ++i)
                ++roll;
            Assert.AreEqual(0, (int)roll);

            for (int i = 0; i < max * 2; ++i)
                --roll;
            Assert.AreEqual(0, (int)roll);

            const int maxex = 1235314;
            RollingInt exag = new RollingInt(maxex);
            for (int i = 0; i < maxex * 3 + 47; ++i)
            {
                exag++;
                int temp = i + 1;
                while (temp >= maxex)
                    temp -= maxex;
                Assert.AreEqual(temp, (int)exag);
            }
            Assert.AreEqual(47, (int)exag);

            RollingInt res = exag - 47;
            Assert.AreEqual(0, (int)res);
            res += 105;
            Assert.AreEqual(105, (int)res);
            res += maxex;
            Assert.AreEqual(105, (int)res);
            res -= maxex;
            Assert.AreEqual(105, (int)res);
        }
        [TestMethod]
        public void TestRollingCollection()
        {
            const int size = 12345;
            RollingCollection<int> roll = new RollingCollection<int>(size, delegate { return 1; });
            for (int i = 0; i < size; ++i)
                Assert.AreEqual(1, roll[i]);
            for (RollingInt rs = new RollingInt(size); !rs.Rolled; ++rs)
                roll[rs] = rs;
            for (int i = 0, l = size-1; i < size; ++i, --l)
            {
                Assert.AreEqual(i, roll[i]);
                roll[i] = l;
            }
            for(RollingInt rs = new RollingInt(size), ms = new RollingInt(size); !rs.Rolled; ++ms)
            {
                --rs;
                Assert.AreEqual((int)ms, roll[rs]);
            }
        }
        [TestMethod]
        public void TestFibonacciRetracement()
        {
            SwingEntry lowa = new SwingEntry()
            {
                BarData = new BarValue<double>()
                {
                    High = 960,
                    Low = 950
                },
                SwingType = SwingState.LowSwing,
                BarLocation = 10
            };
            SwingEntry highb = new SwingEntry()
            {
                BarData = new BarValue<double>()
                {
                    High = 980,
                    Low = 970
                },
                SwingType = SwingState.HighSwing,
                BarLocation = 20
            };

            Assert.AreEqual(980, lowa.FibonacciRetracements(highb, 0));
            Assert.AreEqual(972.92, lowa.FibonacciRetracements(highb, 23.6));
            Assert.AreEqual(968.54, lowa.FibonacciRetracements(highb, 38.2));
            Assert.AreEqual(965, lowa.FibonacciRetracements(highb, 50));
            Assert.AreEqual(961.46, lowa.FibonacciRetracements(highb, 61.8));
            Assert.AreEqual(957.08, lowa.FibonacciRetracements(highb, 76.4));
            Assert.AreEqual(950, lowa.FibonacciRetracements(highb, 100));
            Assert.AreEqual(938.54, lowa.FibonacciRetracements(highb, 138.2));

            Assert.AreEqual(950, highb.FibonacciRetracements(lowa, 0));
            Assert.AreEqual(957.08, highb.FibonacciRetracements(lowa, 23.6));
            Assert.AreEqual(961.46, highb.FibonacciRetracements(lowa, 38.2));
            Assert.AreEqual(965, highb.FibonacciRetracements(lowa, 50));
            Assert.AreEqual(968.54, highb.FibonacciRetracements(lowa, 61.8));
            Assert.AreEqual(972.92, highb.FibonacciRetracements(lowa, 76.4));
            Assert.AreEqual(980, highb.FibonacciRetracements(lowa, 100));
            Assert.AreEqual(991.46, highb.FibonacciRetracements(lowa, 138.2));
        }
    }
}
