#region Using declarations
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Xml.Serialization;
using NinjaTrader.Cbi;
using NinjaTrader.Data;
using NinjaTrader.Gui.Chart;
using ATSI = Automated_Trading_Software.Indicators;
using ATSU = Automated_Trading_Software.Utilities;
#endregion

// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    /// <summary>
    /// Enter the description of your new custom indicator here
    /// </summary>
    [Description("This indicator will measure high / low swings.")]
    public class CustomSwing : Indicator
    {
        //Be carefull, it looks like it doesn't like assignment inside the class in NinjaTrader
        ATSI.SwingIndicator m_swing = new ATSI.SwingIndicator();
        /// <summary>
        /// This method is used to configure the indicator and is called once before any bar data is loaded.
        /// </summary>
        protected override void Initialize()
        {
            Add(new Plot(Color.Black, PlotStyle.Line, "MainPlot"));
            Plots[0].Pen.Width = 2;
            Plots[0].Pen.DashStyle = DashStyle.Solid;
            Overlay = false;
            CalculateOnBarClose = true;
            PriceTypeSupported = false;

            m_swing.Begin();
            m_swing.AddSwingChangeObserver(HandleChange);
        }

        private void HandleChange(ATSU.SwingEntry confirmed)
        {
            int offset = m_swing.CurrentBar;

            double difference, runningValue;
            if (confirmed.SwingType == ATSU.SwingState.LowSwing)
            {
                runningValue = confirmed.BarStart.Low;
                difference = runningValue - confirmed.BarEnd.High;
            }
            else
            {
                runningValue = confirmed.BarStart.High;
                difference = runningValue - confirmed.BarEnd.Low;
            }
            difference /= (double)confirmed.Length;

            for (int i = offset - confirmed.BarStartLocation; i >= offset - confirmed.BarEndLocation; --i)
            {
                Plot0[i] = runningValue;
                runningValue += difference;
            }
        }

        /// <summary>
        /// Called on each bar update event (incoming tick)
        /// </summary>
        protected override void OnBarUpdate()
        {
            ATSU.BarValue<double> LCurrentBar = new ATSU.BarValue<double>() { High = High[0], Low = Low[0] };
            m_swing.BarUpdate(LCurrentBar);
        }

        #region Properties
        [Browsable(false)]	// this line prevents the data series from being displayed in the indicator properties dialog, do not remove
        [XmlIgnore()]		// this line ensures that the indicator can be saved/recovered as part of a chart template, do not remove
        public DataSeries Plot0
        {
            get { return Values[0]; }
        }
        #endregion
    }
}

#region NinjaScript generated code. Neither change nor remove.
// This namespace holds all indicators and is required. Do not change it.
namespace NinjaTrader.Indicator
{
    public partial class Indicator : IndicatorBase
    {
        private CustomSwing[] cacheCustomSwing = null;

        private static CustomSwing checkCustomSwing = new CustomSwing();

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public CustomSwing CustomSwing()
        {
            return CustomSwing(Input);
        }

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public CustomSwing CustomSwing(Data.IDataSeries input)
        {
            if (cacheCustomSwing != null)
                for (int idx = 0; idx < cacheCustomSwing.Length; idx++)
                    if (cacheCustomSwing[idx].EqualsInput(input))
                        return cacheCustomSwing[idx];

            lock (checkCustomSwing)
            {
                if (cacheCustomSwing != null)
                    for (int idx = 0; idx < cacheCustomSwing.Length; idx++)
                        if (cacheCustomSwing[idx].EqualsInput(input))
                            return cacheCustomSwing[idx];

                CustomSwing indicator = new CustomSwing();
                indicator.BarsRequired = BarsRequired;
                indicator.CalculateOnBarClose = CalculateOnBarClose;
#if NT7
                indicator.ForceMaximumBarsLookBack256 = ForceMaximumBarsLookBack256;
                indicator.MaximumBarsLookBack = MaximumBarsLookBack;
#endif
                indicator.Input = input;
                Indicators.Add(indicator);
                indicator.SetUp();

                CustomSwing[] tmp = new CustomSwing[cacheCustomSwing == null ? 1 : cacheCustomSwing.Length + 1];
                if (cacheCustomSwing != null)
                    cacheCustomSwing.CopyTo(tmp, 0);
                tmp[tmp.Length - 1] = indicator;
                cacheCustomSwing = tmp;
                return indicator;
            }
        }
    }
}

// This namespace holds all market analyzer column definitions and is required. Do not change it.
namespace NinjaTrader.MarketAnalyzer
{
    public partial class Column : ColumnBase
    {
        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.CustomSwing CustomSwing()
        {
            return _indicator.CustomSwing(Input);
        }

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public Indicator.CustomSwing CustomSwing(Data.IDataSeries input)
        {
            return _indicator.CustomSwing(input);
        }
    }
}

// This namespace holds all strategies and is required. Do not change it.
namespace NinjaTrader.Strategy
{
    public partial class Strategy : StrategyBase
    {
        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        [Gui.Design.WizardCondition("Indicator")]
        public Indicator.CustomSwing CustomSwing()
        {
            return _indicator.CustomSwing(Input);
        }

        /// <summary>
        /// This indicator will measure high / low swings.
        /// </summary>
        /// <returns></returns>
        public Indicator.CustomSwing CustomSwing(Data.IDataSeries input)
        {
            if (InInitialize && input == null)
                throw new ArgumentException("You only can access an indicator with the default input/bar series from within the 'Initialize()' method");

            return _indicator.CustomSwing(input);
        }
    }
}
#endregion
